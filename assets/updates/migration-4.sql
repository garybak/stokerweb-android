alter table settings add column secureHTTP INTEGER;
alter table settings add column profileName TEXT;
alter table settings add column active INTEGER;
update settings set secureHTTP = 1, profileName = 'Original', active = 1;