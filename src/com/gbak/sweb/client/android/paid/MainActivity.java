package com.gbak.sweb.client.android.paid;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.gbak.sweb.client.android.paid.alarm.AppListener;
import com.gbak.sweb.client.android.paid.convert.DAOConversionUtil;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.SaveDataProcessor;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.service.FetchDataService;
import com.gbak.sweb.client.android.paid.services.BaseAsyncTask;
import com.gbak.sweb.client.android.paid.services.StokerWebDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.DeviceDataList;

import com.gbak.sweb.common.json.Probe;

import com.gbak.sweb.client.android.paid.R;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
  //  ArrayList<String> listItems = new ArrayList<String>();
    ProbeItemAdapter adapter = null;
    DeviceDataList deviceDataList = new DeviceDataList();
    AtomicBoolean gettingData = new AtomicBoolean(false);
    ImageButton refreshButton = null;
 //   List<SettingsDAO> settings;
    boolean dataUpdated = false;
    boolean noActiveSetting = false;
    boolean nightModeEnabled = false;
    
    private String empty_list_message = "Unable to retrieve data";
    
    public static final String ACTION_NAME = "com.gbak.sweb.client.android.paid.UpdateAlarmAction";
    private IntentFilter myFilter = new IntentFilter(ACTION_NAME);
    
    int dataFailures = 0;
    
    Context context = null;
    Activity currentActivity  = null;
    private static final String ACTIVITY_RECEIVER_SAVE_START = "SaveStart";
    private static final String ACTIVITY_RECEIVER_SAVE_END = "SaveEnd";
    
    boolean savingData = false;
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE); // Remove Title bar
        
        context = this;
        currentActivity = this;
        
        setContentView(R.layout.activity_main); 
        
        refreshButton = (ImageButton) findViewById(R.id.main_button_refresh);
        setupSoftRefresh();
        
        TextView empty = (TextView) findViewById(R.id.empty_list);
        empty.setText(empty_list_message);
        
        SettingsDAO s;
        try
        {
            s = DatabaseManager.getInstance().getActiveSettings();
        }
        catch (NoActiveSettingException e)
        {
            loadSettingsActivity();
            return;
        }
        if ( s == null )
        {
            loadSettingsActivity();
            return;
        }
        
        
        new ProbeDataTask().execute(); //"Loading...");
        
        createUpdateAlarm();
        
        createBackgroundAlarm();
        
        if (activityReceiverSaveStart != null) 
        {
                      IntentFilter intentFilter = new IntentFilter(ACTIVITY_RECEIVER_SAVE_START);
                      registerReceiver(activityReceiverSaveStart, intentFilter);
        }
        
        if (activityReceiverSaveEnd != null) 
        {
                      IntentFilter intentFilter = new IntentFilter(ACTIVITY_RECEIVER_SAVE_END);
                      registerReceiver(activityReceiverSaveEnd, intentFilter);
        }

    }

    private void createBackgroundAlarm()
    {
        WakefulIntentService.scheduleAlarms(new AppListener(), this, true);
    }
    private void cancelBackgroundAlarm()
    {
        WakefulIntentService.cancelAlarms(this);
        
    }
    
    private BroadcastReceiver activityReceiverSaveStart = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) 
        {
            savingData = true;
            cancelUpdateAlarm();
            cancelBackgroundAlarm();
            startRefreshAnimation();
            
        }
    };

    private BroadcastReceiver activityReceiverSaveEnd = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) 
        {
            savingData = false;
            FetchDataService.clearLastRetreivedDate();
            new ProbeDataTask().execute( );
            createUpdateAlarm();
            createBackgroundAlarm();
            stopRefreshAnimation();
        }
    };

    public boolean isSaveInProgress()
    {
        return savingData;
    }
    
    private void hardRefresh()
    {
            try
            {
                if (StokerWebAndroid.getServices().stokerWebDataService == null )
                   StokerWebAndroid.refreshServices();
            }
            catch (NoActiveSettingException e)
            {
                loadSettingsActivity();
            }
          
          finish();
          FetchDataService.clearLastRetreivedDate();
          startActivity(getIntent());
    }
    
    private void setupSoftRefresh()
    {
       refreshButton.setOnClickListener(new OnClickListener() 
       {
            public void onClick(View v) 
            {
               Log.d(TAG,"Refresh button pressed");
               cancelUpdateAlarm();
               startRefreshAnimation();
               FetchDataService.clearLastRetreivedDate();
               new ProbeDataTask().execute( );
               createUpdateAlarm();
               
            }
            });
    }
    
    private void startRefreshAnimation()
    {
        runOnUiThread(new Runnable() 
        {
            public void run() 
            {

                Animation rotate = AnimationUtils.loadAnimation(context, R.anim.rotate_indefinitely);
                rotate.setInterpolator(new LinearInterpolator());
                refreshButton.startAnimation(rotate);
           }
       });
        
    }
    
    private void stopRefreshAnimation()
    {
        runOnUiThread(new Runnable() 
        {
            public void run() 
            {
                if ( refreshButton.getAnimation() != null )
                    refreshButton.getAnimation().cancel();
           }
       });
    }
    protected void createUpdateAlarm()
    {
        registerReceiver( alarmReceiver, myFilter );
        dataFailures = 0;

        Intent intent = new Intent(ACTION_NAME);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        
        
        // TODO: fix this:  Add the values to the properties file or make them configurable in the options
        int Interval = 5000;
        //if ( deviceDataList.devices.size() > 0 &&  deviceDataList.devices.get(0).cooker.length() == 0)
  /*      if ( settings.get(0).getServerConnectionType().compareTo(ServerConnectionType.STOKER.toString()) == 0 )
        {
           Interval = 120000; 
        }*/
        AlarmManager alarmMgr = (AlarmManager)getSystemService(Activity.ALARM_SERVICE ); // Context.ALARM_SERVICE);
        alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() , Interval, pendingIntent);

    }
    

    private void loadSettingsActivity()
    {
       // Intent intent = new Intent( getBaseContext(), SettingsActivity.class);
        Intent intent = new Intent( getBaseContext(), ConnectionProfile.class);
      //  finish();
        startActivityForResult( intent,0 );
        
    }
    
    @Override
    protected void onResume() {
       //registerReceiver(alarmReceiver, myFilter);
        createUpdateAlarm();
        super.onResume();
    }
    
    private void cancelUpdateAlarm()
    {
        try
        {
           unregisterReceiver(alarmReceiver);
        }
        catch(IllegalArgumentException e)
        {
            Log.w(TAG, "Exception calling unregisterReceiver(alarmReceiver) in onPause()");
        }
    }
    @Override
    public void onPause()
    {
        cancelUpdateAlarm();
        
        super.onPause();
    }
    
    @Override 
    public void onStop()
    {
        super.onStop();
    }
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        try
        {
            unregisterReceiver(activityReceiverSaveStart);
        }
        catch(IllegalArgumentException e)
        {
            Log.w(TAG, "Exception calling unregisterReceiver(activityReceiverSaveStart) in onDestroy()");
        }
    
        try
        {
           unregisterReceiver(activityReceiverSaveStart);
           unregisterReceiver(activityReceiverSaveEnd);
        }
        catch(IllegalArgumentException e)
        {
            Log.w(TAG, "Exception calling unregisterReceiver(activityReceiverSaveEnd) in onDestroy()");
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        if ( item.getItemId() == R.id.menu_settings)
        {
            Log.d(getLocalClassName(),"menu settings selected");
            loadSettingsActivity();
        }
        else if ( item.getItemId() == R.id.itemRefresh)
        {
            hardRefresh();
        }
        else if (item.getItemId() == R.id.night_mode)
        {
            enableNightMode();
        }
        else if (item.getItemId() == R.id.main_menu_stopAll)
        {
            stopAll();
        }
        else if ( item.getItemId() == R.id.sendLog )
        {
           new LogcatAsyncTask().execute();
        }
        
/*        switch ( item.getItemId())
        {
            case R.id.menu_settings:
            {
                Log.d(getLocalClassName(),"menu settings selected");
                loadSettingsActivity();
                break;
            }
            case R.id.itemRefresh:
            {
                hardRefresh();
            }
            case R.id.main_menu_stopAll:
            {
                stopAll();
            }
        }*/
        return false;
        
    }
    
    private void enableNightMode()
    {
        if ( nightModeEnabled == false )
        {
            currentActivity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            WindowManager.LayoutParams lparams = getWindow().getAttributes();   
            lparams.screenBrightness=0.1f;
            currentActivity.getWindow().setAttributes(lparams);
            nightModeEnabled = true;
        }
        else
        {
            currentActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            WindowManager.LayoutParams lparams = getWindow().getAttributes();  
            
            lparams.screenBrightness=1.0f;
            currentActivity.getWindow().setAttributes(lparams);
            nightModeEnabled = false;
        }
        
        /*
         * PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
           PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Tag");
           wl.acquire();
            ....
           wl.release();

         */
        
        //Disable: getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
    private void stopAll()
    {
        WakefulIntentService.cancelAlarms(this);
        finish();
    }
    private void updateProbeData()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");   
        TextView updateTime = (TextView) findViewById(R.id.updateTime);
        String formattedDate = "--:--";
        if ( deviceDataList != null && deviceDataList.receivedDate != null )
           updateTime.setText(dateFormat.format( deviceDataList.receivedDate));
        
       adapter.update( deviceDataList );
       
       if ( dataUpdated )
       {
           adapter.notifyDataSetChanged();
           dataUpdated = false;
       }

       
    }
    
    private void loadProbeData()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");    
        TextView updateTime = (TextView) findViewById(R.id.updateTime);
        if ( deviceDataList.receivedDate != null)
           updateTime.setText(dateFormat.format( deviceDataList.receivedDate));
        
       
        adapter = new ProbeItemAdapter(this, android.R.layout.simple_list_item_1, deviceDataList);
        ListView listView = (ListView) findViewById(R.id.listView1);

        listView.setAdapter(adapter);
        
        listView.setOnItemClickListener( new OnItemClickListener() 
        {

            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3)
            {
               if ( savingData == true )
                   return;
               
               Log.d("tag","item clicked");
               //Intent detailIntent = new Intent( arg1.getContext(),ProbeDetail.class);
               Intent detailIntent = new Intent( arg1.getContext(),ProbeDetailPager.class);
               
               Device d = deviceDataList.devices.get(arg2);
               String cooker = d.cooker;
               ArrayList<String> deviceIdList = new ArrayList<String>();
               
               // Update the device in the database table.  This will be used in the next
               // activity

               DatabaseManager.getInstance().clearDeviceStash();
               for ( Device dl : deviceDataList.devices )
               {
                   if ( dl.id == null )
                       continue;  // cooker spacer
                   
                //   if ( dl.cooker.compareTo(cooker) == 0)   // This permitted only probes from one cooker from appearing 
                                                              // on the detail page.  It's best to have all available
                                                              // via side swipe since an update to the stoker updates all of them.
                //   {
                      DeviceStashDAO dao = DAOConversionUtil.convertToDeviceStashDAO(dl);
                      DatabaseManager.getInstance().updateDeviceStash(dao);
                      deviceIdList.add( dao.getId() );
                //   }
               }
               
               // TODO: blower enhancement
              /* for ( Device dl : deviceDataList.availableBlowers )
               {
                   DeviceStashDAO dao = DAOConversionUtil.convertToDeviceStashDAO(dl);
                   DatabaseManager.getInstance().updateDeviceStash(dao);
               }*/
               
               detailIntent.putStringArrayListExtra("deviceList", deviceIdList);  // Push probe data to detail screen.
               
               detailIntent.putExtra("selectedID", ((Probe)d).id);

               startActivity(detailIntent);
            }
        });
        
        listView.setEmptyView( (TextView) findViewById( R.id.empty_list));
    }
    
    BroadcastReceiver alarmReceiver = new BroadcastReceiver() 
    {
        @Override
        public void onReceive(Context context, Intent intent) 
        {
           // boolean updateRequired = FetchDataService.updateRequired("");
           // if ( updateRequired )
            if ( noActiveSetting == true )
            {
                loadSettingsActivity();
                noActiveSetting = false;
            }
            else
               new ProbeDataTask().execute( );

        }
    };
    /*
    public class UpdateAlarmReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Toast.makeText(context, "Alarm went off", Toast.LENGTH_SHORT)
                    .show();
        }
    }
    */
    
    // http://www.fattybeagle.com/2011/02/15/android-asynctasks-during-a-screen-rotation-part-ii/
    
    private class ProbeDataTask extends AsyncTask<Void, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(Void... arg0)
        {
           
            if ( gettingData.compareAndSet(false,true))
            {
                
//                AndroidDatabaseResults results = (AndroidDatabaseResults) DatabaseManager.getInstance().getDeviceIterator().getRawResults();
//                Cursor cursor = results.getRawCursor();
//               
                //StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;

                boolean connect = false;
                try
                {
                 //  deviceDataList = swData.getTempData();
                    boolean updateRequired = FetchDataService.updateRequired("");
                    
                    if ( deviceDataList.devices.size() == 0 )  // Forces update when activity recreated, on rotation
                        updateRequired = true;
                    
                   if ( updateRequired == true )
                   {
                       startRefreshAnimation();
                   //   Log.d(TAG,"Backgroud Request");
                       deviceDataList = FetchDataService.getData(false);
                       dataUpdated = true;
                   }
                       connect = true;
                }
                catch ( UnableToConnectException uce )
                {
                    stopRefreshAnimation();
                    uce.printStackTrace();
                    Log.e("MainActivity","Can't make connection to fetch data");
                    
                }
                catch ( NoActiveSettingException nase)
                {
                    stopRefreshAnimation();
                    Log.e(TAG,"No Active Setting Configured");
                    noActiveSetting = true;
                }
                

                
                if ( connect == false)
                {
                    // Failure getting data
                    dataFailures++;
                    if ( dataFailures > 5 )
                    {
                        unregisterReceiver(alarmReceiver);
              //          Toast.makeText(StokerWebAndroid.getContext(), "Unable to make connection",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                   dataFailures = 0;
                
               // gettingData.set(false);
                return true;
            }
            return false;
        }
        
        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
            {
                try 
                {
                   // Log.d(TAG,"Backgroud Response");
                    if ( adapter == null )
                       loadProbeData();
                    else
                        updateProbeData();
                    
                }
                catch (Throwable t) 
                {
                    t.printStackTrace();
                }
                finally
                {
                    stopRefreshAnimation();
                    gettingData.set(false);
                }
            }
        }
        
    }
    
    private class LogcatAsyncTask extends AsyncTask<Integer, Void, String> 
    {
       @Override
       protected String doInBackground(Integer... ids) 
       {
          try
          {
             Process process = Runtime.getRuntime().exec("logcat -v time -d");
             BufferedReader bufferedReader = new BufferedReader(
                   new InputStreamReader(process.getInputStream()));

             FileOutputStream fileOutputStream = openFileOutput(AppConstants.LoggingZipfileOutputFilename, Context.MODE_PRIVATE); 
             ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);

             String line = "";

             ZipEntry ze = new ZipEntry(AppConstants.LoggingLogcatOutputFilename);
             zipOutputStream.putNextEntry(ze);
             
             while ((line = bufferedReader.readLine()) != null)
             {
                zipOutputStream.write(line.getBytes());
                zipOutputStream.write('\n');
             }
             zipOutputStream.flush();
             bufferedReader.close();
             
            
             
            /* // Write Device info file
             {
                ZipEntry ze3 = new ZipEntry(AppConstants.LoggingDeviceInformationOutputFilename);
                zipOutputStream.putNextEntry(ze3);
                
                zipOutputStream.write(com.unicoi.instavoip.android.softphone.debug.DebugUtils.getDeviceDebugInfo(getActivity()).getBytes());
                zipOutputStream.flush();
             }*/
             
             zipOutputStream.close();
             fileOutputStream.close();
             
             return AppConstants.LoggingZipfileOutputFilename;
          }
          catch (IOException e)
          {
             Log.e(TAG,"IOException trying to create debug info file.", e);
          }
          return "";
       }
       @Override
       protected void onPostExecute(final String result)
       {

          Uri U = Uri.parse("content://com.gbak.sweb.client.android.paid/" + result);
          Intent emailIntent = new Intent(Intent.ACTION_SEND);
          emailIntent.setType("application/zip");
          emailIntent.putExtra(Intent.EXTRA_STREAM, U);
          emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"stokerwebhelp@gmail.com"});
          emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "StokerWeb logs for " + android.os.Build.MODEL);
          emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Log created on: " + Calendar.getInstance().getTime().toString());

          startActivity(Intent.createChooser(emailIntent,"Email:"));
          
       }
    }
    
}
