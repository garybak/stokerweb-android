package com.gbak.sweb.client.android.paid;

import java.util.ArrayList;
import java.util.List;

import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.constants.ServerConnectionType;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.service.FetchDataService;
import com.gbak.sweb.client.android.paid.services.StokerWebDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.R;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;


public class SettingsActivity extends Activity
{
    private static final String TAG = "SettingsActivity";
    private Button buttonSave = null;
    private Button buttonCancel = null;
    EditText textProfileName = null;
    AutoCompleteTextView textHostIP = null;
    EditText textPort = null;
    EditText textBaseURL = null;
    EditText textUsername = null;
    EditText textPassword = null;
    CheckBox secureHTTP = null;
    TextView testUrl = null;
    ProgressBar testProgress = null;
    
    EditText textRingtone = null;
    
    Ringtone ringtone = null;
    RingtoneManager m_RingtoneManager = null;
    Cursor m_RingtoneCursor;
    Button buttonSetRingTone = null;
    Intent intentRingtone = null;
    Uri ringtoneURI = null;
    
    private Button buttonTest = null;
    
    SettingsDAO settings = null;
    ServerConnectionType connectionType;
    RadioButton radioButtonStoker;
    RadioButton radioButtonStokerWeb;
            
    RadioGroup rg = null;
    
    boolean newSettings = false;
    
    Context context = null;
    
    private static enum connType { STOKER, STOKER_WEB };
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        context = this;
        DatabaseManager.init(this);

        buttonSave = (Button) findViewById(R.id.settings_button_save);
        buttonSave.setOnClickListener( saveHandler );
        
        buttonCancel = (Button) findViewById(R.id.settings_button_cancel);
        buttonCancel.setOnClickListener( cancelHandler );
        
        buttonTest = (Button) findViewById(R.id.settings_button_test);
        buttonTest.setOnClickListener(testHandler);
        
        textProfileName = (EditText) findViewById( R.id.settings_profileName );
        
        textHostIP = (AutoCompleteTextView) findViewById( R.id.settings_hostIP);
        
        textPort = (EditText) findViewById( R.id.settings_port);
        textBaseURL = (EditText) findViewById( R.id.settings_baseURL );
        
        textUsername = (EditText) findViewById( R.id.settings_user );
        textPassword = (EditText) findViewById( R.id.settings_password );
        
        textRingtone = (EditText) findViewById( R.id.settings_textNotificationSound);
        buttonSetRingTone = (Button) findViewById( R.id.settings_ButtonNotificationSound);
        buttonSetRingTone.setOnClickListener(pickNotificationSound);
        
        rg = (RadioGroup) findViewById( R.id.radioGroupConnectionType);;
        int checkedId = rg.getCheckedRadioButtonId();

        radioButtonStoker = (RadioButton) findViewById(R.id.radioButtonStoker);
        radioButtonStokerWeb = (RadioButton) findViewById(R.id.radioButtonStokerweb);
        
        radioButtonStoker.setOnCheckedChangeListener(new OnCheckedChangeListener() 
        {
          //  @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1)
            {
                if ( radioButtonStoker.isChecked())
                    toggleConnectionType(SettingsActivity.connType.STOKER); 
            }
        });
        
        radioButtonStokerWeb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

        //    @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1)
            {
                if ( radioButtonStokerWeb.isChecked())
                    toggleConnectionType(SettingsActivity.connType.STOKER_WEB);
                
            }
            
        });
        
        secureHTTP = (CheckBox) findViewById( R.id.settings_checkBoxSecureHTTP);
        
        testUrl = (TextView) findViewById( R.id.settings_test_url);
        testUrl.setSelectAllOnFocus(true);
        
        testProgress = (ProgressBar) findViewById( R.id.progressBar1);
        testProgress.setVisibility(View.INVISIBLE);
               
        toggleConnectionType(SettingsActivity.connType.STOKER);
                
        String settingsID = getIntent().getExtras().getString("settingsID");
       if ( settingsID == null || settingsID.length() == 0)
           return;
       
       settings = DatabaseManager.getInstance().getSettingsRow(settingsID); //settingsDataSource.getSettings();
        //if ( values.size() > 0 )
        //   settings = values.get(0);
        
        if ( settings == null )
            return;
        
        if ( settings.getProfileName() != null )
            textProfileName.setText( settings.getProfileName());
        
        if ( settings.getHostname() != null )
           textHostIP.setText(settings.getHostname());
         
        if ( settings.getPort() != null )
           textPort.setText(settings.getPort());
        
        if ( settings.getUserName() != null )
            textUsername.setText( settings.getUserName() );
        
        if ( settings.getPassword() != null )
            textPassword.setText( settings.getPassword() );
        
        if ( settings.getBaseURL() != null)
            textBaseURL.setText( settings.getBaseURL());
        
        if ( settings.getServerConnectionType() != null )
        {
            String connectionType = settings.getServerConnectionType();
            if ( connectionType.compareTo(ServerConnectionType.STOKER.toString() ) == 0)
            {
                radioButtonStoker.setChecked(true);
            }
            else
            {
                radioButtonStokerWeb.setChecked(true);
            }
                
        }
        else
            radioButtonStokerWeb.setChecked(true);
        
        secureHTTP.setChecked(settings.isSecureHTTP());
    
        if ( settings.getRingerURI() != null && settings.getRingerURI().length() > 0)
        {
            ringtoneURI = Uri.parse(settings.getRingerURI());
                    
        }
        else
        {
            // get Default notification sound
            ringtoneURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        }
        
        Ringtone ringtone = RingtoneManager.getRingtone(context, ringtoneURI);
        String name = "";
        
        if ( ringtone != null )
           name = ringtone.getTitle(context);
        
        textRingtone.setText(name);
        
        
        m_RingtoneManager = new RingtoneManager(this);
        //m_RingtoneCursor = m_RingtoneManager.getCursor();
        //String title = RingtoneManager.EXTRA_RINGTONE_TITLE;
        
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, new ArrayList<String>());
        textHostIP.setAdapter(adapter);
        
    }
    
    private void testConnection()
    {
        
        new TestTask().execute();

        
    }
    
    private void toggleConnectionType(connType ct)
    {
        boolean enabled = false;
        if ( ct == SettingsActivity.connType.STOKER )
        
            enabled = false;
        else
            enabled = true;
        
        int checkedId = rg.getCheckedRadioButtonId();
        secureHTTP.setEnabled( enabled );
        textUsername.setEnabled(enabled);
        textPassword.setEnabled(enabled);
        textBaseURL.setEnabled(enabled);

    }
    
    private void setSettings()
    {
 
        String profileName = textProfileName.getText().toString();
        String hostname = textHostIP.getText().toString();
        String port = textPort.getText().toString();
        String username = textUsername.getText().toString();
        String password = textPassword.getText().toString();
        String baseURL = textBaseURL.getText().toString();
        String serverConnectionType;  
        if ( radioButtonStoker.isChecked())
            serverConnectionType = ServerConnectionType.STOKER.toString(); 
        else
            serverConnectionType = ServerConnectionType.STOKER_WEB.toString();
        boolean https = secureHTTP.isChecked();
        
        String ringtoneUriString = "";
        
        if ( ringtoneURI != null )
           ringtoneUriString = ringtoneURI.toString();
        
        
        if ( settings == null )
        {
            newSettings = true;
           settings = new SettingsDAO(0, hostname, port, username, password, baseURL, serverConnectionType, https, profileName, false, ringtoneUriString );     
        }
        else
        {

            settings.setHostname(hostname);
            settings.setPort(port);
            settings.setUserName(username);
            settings.setPassword(password);
            settings.setBaseURL(baseURL);
            settings.setServerConnectionType(serverConnectionType);
            settings.setSecureHTTP(https);
            settings.setRingerURI(ringtoneUriString);
        }

        
        
    }
    
    private OnClickListener saveHandler = new OnClickListener()
    {

        public void onClick(View v)
        {

            setSettings();
            if ( newSettings)
                DatabaseManager.getInstance().addSettings( settings );
            else
                DatabaseManager.getInstance().updateSettings(settings);
           
            finish();
            
        }

    };
    
    private OnClickListener cancelHandler = new OnClickListener()
    {
        public void onClick(View v) {
            finish();
        }
    };
    
    private OnClickListener testHandler = new OnClickListener()
    {
        public void onClick(View v) 
        {
            setSettings();
            testConnection();
        }
    };
    
    private void pickCustomSound()
    {
        intentRingtone = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);

        intentRingtone.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALL);

        //gives the title of the RingtoneManager picker title
      //  intentRingtone.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Choose a sound!");

        //returns true shows the rest of the songs on the device in the default location
      //  intentRingtone.getBooleanExtra(RingtoneManager.EXTRA_RINGTONE_INCLUDE_DRM, true);

        intentRingtone.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, android.provider.Settings.System.DEFAULT_NOTIFICATION_URI );
        intentRingtone.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
        
        String uri = null;
        //chooses and keeps the selected item as a uri
       /* if ( uri != null ) 
        { 
           intentRingtone.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse( uri ));
        } 
        else 
        { 
           intentRingtone.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri)null);
        }*/
        
        startActivityForResult(intentRingtone, 0);
    
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        if (requestCode == 0) 
        {
            if (resultCode == RESULT_OK) 
            {
         
                String ringtoneString;
                
                Uri uri = data.getParcelableExtra(m_RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            //    data.getParcelableExtra(RingtoneManager.EX)
              String test = uri.toString();
              //prints out the result in the console window
              Log.i(TAG, "Chosen URI: " + uri);
              ringtoneURI = uri;
              
              try {
                  ringtoneString = RingtoneManager.getRingtone(this, uri).getTitle(this);

              } catch (final Exception e) {
                  ringtoneString = "unknown";
              }
              
              textRingtone.setText(ringtoneString);
              
            }
        }
        
    }
    
    private OnClickListener pickNotificationSound = new OnClickListener()
    {
        public void onClick( View v )
        {
            pickCustomSound();
        }
    };
    
    private void showProgress(final String testAddress)
    {
        runOnUiThread(new Runnable() 
        {
            public void run() 
            {
                testUrl.setText( testAddress );
                
                buttonTest.setVisibility(View.INVISIBLE);
                testProgress.setVisibility(View.VISIBLE);
                testProgress.setIndeterminate(true);
                
             //   Animation rotate = AnimationUtils.loadAnimation(context, R.anim.rotate_indefinitely);
               // rotate.setInterpolator(new LinearInterpolator());
              //  testProgress.startAnimation(rotate);
                
           }
       });
    }
    
    private void stopProgress(final String message)
    {
        runOnUiThread(new Runnable() 
        {
            public void run() 
            {
                buttonTest.setVisibility(View.VISIBLE);
                testProgress.setVisibility(View.INVISIBLE);
     
                
                if ( testProgress.getAnimation() != null )
                    testProgress.getAnimation().cancel();
                
                if ( message != null )
                    if ( message.compareTo("Success") == 0)
                    {
                        testUrl.setTextColor(Color.GREEN);
                    }
                    else
                    {
                        testUrl.setTextColor(Color.RED);
                    }
           }
       });

    }
    
    private class TestTask extends AsyncTask<Void, Void, String>
    {
        @Override
        protected String doInBackground(Void... arg)
        {
            StokerWebDataService stokerWebDataService = null;
            
            if (   settings.getServerConnectionType().compareTo(ServerConnectionType.STOKER.toString()) == 0 )
                stokerWebDataService = new com.gbak.sweb.client.android.paid.services.stoker.StokerWebDataService();
            else
                stokerWebDataService = new com.gbak.sweb.client.android.paid.services.server.StokerWebDataService();
            
            String testAddress = stokerWebDataService.getTestAddress( settings );
            
   
            showProgress(testAddress);
            String message = stokerWebDataService.testConnection( testAddress );
         
            return message;
        }
        
        @Override
        protected void onPostExecute(String message)
        {
           stopProgress(message);
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
        
    }
    
    
}
