package com.gbak.sweb.client.android.paid;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.gbak.sweb.client.android.paid.convert.DAOConversionUtil;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.dao.DeviceDAO;
import com.gbak.sweb.client.android.paid.service.FetchDataService;
import com.gbak.sweb.client.android.paid.services.BaseAsyncTask;
import com.gbak.sweb.client.android.paid.services.StokerWebDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;

import com.gbak.sweb.client.android.paid.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

public class LogActivity extends Activity 
{

    private LogListGroupAdapter logListAdapter;
    private LogItemList logItemList;
    private ExpandableListView expandListView;
    AtomicBoolean gettingData = new AtomicBoolean(false);   
    boolean dataUpdated = false;  
    int dataFailures = 0;
    String cookerName = "";
    
    public static final String ACTION_NAME = "com.gbak.sweb.client.android.paid.UpdateLogAlarmAction";
    private IntentFilter myFilter = new IntentFilter(ACTION_NAME);
    
    private final static String TAG = "LogActivity";
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
       
        expandListView = (ExpandableListView) findViewById(R.id.expandableListViewLogs);
      //  logItemList = SetStandardGroups();
        cookerName = getIntent().getStringExtra("cookerName");
        
        setTitle(cookerName + " Logs");
        createUpdateAlarm();
        
        
    //    logListAdapter = new LogListGroupAdapter(LogActivity.this, logItemList.logList);
     //   expandListView.setAdapter(logListAdapter);
    }

    
    
    protected void createUpdateAlarm()
    {
        registerReceiver( alarmReceiver, myFilter );
        dataFailures = 0;

        Intent intent = new Intent(ACTION_NAME);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        
        
        // TODO: fix this:  Add the values to the properties file or make them configurable in the options
        int Interval = 5000;
        //if ( deviceDataList.devices.size() > 0 &&  deviceDataList.devices.get(0).cooker.length() == 0)
  /*      if ( settings.get(0).getServerConnectionType().compareTo(ServerConnectionType.STOKER.toString()) == 0 )
        {
           Interval = 120000; 
        }*/
        AlarmManager alarmMgr = (AlarmManager)getSystemService(Activity.ALARM_SERVICE ); // Context.ALARM_SERVICE);
        alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() , Interval, pendingIntent);

    }
    
    @Override
    protected void onResume() {
       //registerReceiver(alarmReceiver, myFilter);
        createUpdateAlarm();
        super.onResume();
    }
    
    private void unregisterAlarmReceiver()
    {
        try
        {
           unregisterReceiver(alarmReceiver);
        }
        catch(IllegalArgumentException e)
        {
            Log.w(TAG, "Exception calling unregisterReceiver(alarmReceiver) in onPause()");
        }
    }
    
    @Override
    public void onPause()
    {
        unregisterAlarmReceiver();
        super.onPause();
    }
    
    @Override 
    public void onStop()
    {
        super.onStop();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        getMenuInflater().inflate(R.menu.activity_log, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        if ( item.getItemId() == R.id.new_log )
        {
            newLog();
        }/*
        switch ( item.getItemId())
        {
            case R.id.new_log:
            {
                newLog();
                break;
            }

        }
*/        return false;
        
    }
    
    public void newLog()
    {
   
        List<DeviceDAO> deviceDAO = DatabaseManager.getInstance().getDevicesForCooker(cookerName);
        
        final List<Device> groups  = DAOConversionUtil.convertToList(deviceDAO);
        
        final HashMap<Integer,Device> selectedItemHash = new HashMap<Integer,Device>();
       
        final EditText input = new EditText(this);
        input.setSingleLine();
        input.setHint("Log Name");
       
        int i = 0;
        boolean[] selectedArray = new boolean[groups.size()];
        CharSequence[] groupArray = new CharSequence[groups.size()];
        for ( Device g : groups )
        {
            groupArray[i] = g.Name;
            selectedArray[i] = false;
            i++;
        }
         
        new AlertDialog.Builder(this)
        .setTitle("New Log")
        .setView(input)
        .setMultiChoiceItems(groupArray, selectedArray, new DialogInterface.OnMultiChoiceClickListener() {
        
            public void onClick(DialogInterface dialog, int which,
                    boolean isChecked) {
                
                if (isChecked) 
                {
                    selectedItemHash.put(Integer.valueOf(which), groups.get(which));
                } 
                else if (selectedItemHash.containsKey(Integer.valueOf(which))) 
                {
                    selectedItemHash.remove(Integer.valueOf(which));
                }
                
            }
            
        })
        .setPositiveButton("Save", new DialogInterface.OnClickListener() 
        {
            public void onClick(DialogInterface dialog, int whichButton) 
            {
                ArrayList<Device> logList = new ArrayList<Device>();
                for ( Device li : selectedItemHash.values())
                {
                   logList.add( li );    
                }
                
                LogItem logItem = new LogItem(input.getText().toString(),cookerName,Calendar.getInstance().getTime(),logList );
                LogItem[] lna = new LogItem[] { logItem };
                new EndLogTask().execute(lna);
                // COnvert the selectedItems array to LogItem list and submit
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
        {
            public void onClick(DialogInterface dialog, int whichButton) 
            {
                // Do nothing.
            }
        }).create().show();
             
    }


    
    BroadcastReceiver alarmReceiver = new BroadcastReceiver() 
    {

        @Override
        public void onReceive(Context context, Intent intent) 
        {
            new ProbeDataTask().execute(LogActivity.this, null );
             
        }
    };
    
    private void loadProbeData()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");    
        TextView updateTime = (TextView) findViewById(R.id.updateTime);
     //   if ( deviceDataList.receivedDate != null)
     //      updateTime.setText(dateFormat.format( deviceDataList.receivedDate));
        
        logListAdapter = new LogListGroupAdapter(LogActivity.this, logItemList.logList);
        expandListView.setAdapter(logListAdapter);

        /*
        expandListView.setOnItemClickListener( new OnItemClickListener() 
        {

            
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3)
            {
               
               Log.d("tag","item clicked");
               //Intent detailIntent = new Intent( arg1.getContext(),ProbeDetail.class);
               Intent detailIntent = new Intent( arg1.getContext(),ProbeDetailPager.class);
               
               Device d = deviceDataList.devices.get(arg2);
               
               String cooker = d.cooker;
               ArrayList<String> deviceIdList = new ArrayList<String>();
               
               // Update the device in the database table.  This will be used in the next
               // activity

               DatabaseManager.getInstance().clearDeviceStash();
               for ( Device dl : deviceDataList.devices )
               {
                   if ( dl.id == null )
                       continue;  // cooker spacer
                   
                   if ( dl.cooker.compareTo(cooker) == 0)
                   {
                      DeviceStashDAO dao = DAOConversionUtil.convertToDeviceStashDAO(dl);
                      DatabaseManager.getInstance().updateDeviceStash(dao);
                      deviceIdList.add( dao.getId() );
                   }
               }
               
               detailIntent.putStringArrayListExtra("deviceList", deviceIdList);
               
               detailIntent.putExtra("selectedID", ((Probe)d).id);

               startActivity(detailIntent);
            }
        });
        */
        
     //   listView.setEmptyView( (TextView) findViewById( R.id.empty_list));
        
    }
    
    private void updateProbeData()
    {
        logListAdapter.update(logItemList.logList);
        logListAdapter.notifyDataSetChanged();
    }
    
    private class EndLogTask extends AsyncTask<LogItem, Void, Boolean>
    {
        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
                Toast.makeText(StokerWebAndroid.getContext(), "Save Successful", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(StokerWebAndroid.getContext(), "Save Failed", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(LogItem... li)
        {
            try
            {
                StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;
                swData.newLog(li[0]);
            }
            catch (UnableToConnectException uce)
            {
                Log.e("EndLogTask","Unable to make connection to End Log");
                return false;
            }
            catch (NoActiveSettingException e)
            {
                Log.e("EndLogTask","No active setting detected when attempting to End Log");
                return false;
            }
            return true;
        }
    }

    
    private class ProbeDataTask extends BaseAsyncTask<String, Void> 
    {
        protected Boolean doWorkInBackground(String... creds) throws Throwable 
        {
            if ( gettingData.getAndSet(true) == false )
            {
//                AndroidDatabaseResults results = (AndroidDatabaseResults) DatabaseManager.getInstance().getDeviceIterator().getRawResults();
//                Cursor cursor = results.getRawCursor();
//               
                //StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;

                boolean connect = false;
                try
                {
                 //  deviceDataList = swData.getTempData();
                    boolean updateRequired = FetchDataService.updateRequired(cookerName);
                   if ( updateRequired == true || logItemList == null )
                   {
                       logItemList = FetchDataService.getLogData(false, cookerName );
                       dataUpdated = true;
                   }
                       connect = true;
                }
                catch ( UnableToConnectException uce )
                {
                    uce.printStackTrace();
                    Log.e("MainActivity","Can't make connection to fetch data");
                }
                if ( connect == false)
                {
                    // Failure getting data
                    dataFailures++;
                    if ( dataFailures > 5 )
                    {
                        unregisterAlarmReceiver();
                        Toast.makeText(StokerWebAndroid.getContext(), "Unable to make connection",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                   dataFailures = 0;
                
                gettingData.set(false);
            }
            return true;
        }

        protected void onWorkPostExecute(Boolean result) 
        {
            if (result)
            {
                try 
                {
                    if ( logListAdapter == null )
                       loadProbeData();
                    else
                        updateProbeData();
                    
                }
                catch (Throwable t) 
                {
                    
                    t.printStackTrace();
                }
            }
        }
    }
}
