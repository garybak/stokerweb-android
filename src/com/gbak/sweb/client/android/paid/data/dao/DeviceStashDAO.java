package com.gbak.sweb.client.android.paid.data.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="device_stash")
public class DeviceStashDAO extends DeviceDAO
{
    @DatabaseField( defaultValue = "false")
    private boolean updated;

    public DeviceStashDAO() { } 
    
    public DeviceStashDAO( DeviceDAO d)
    {
        super(d);
    }
    
    public boolean isUpated()
    {
        return updated;
    }

    public void setUpated(boolean upated)
    {
        this.updated = upated;
    }
    
}
