package com.gbak.sweb.client.android.paid.data.dao;

import java.util.Date;

import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.base.constant.DeviceType;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="device_state")
public class DeviceDAO
{
    
    static int ALERT_ON_TARGET = 1;
    static int ALERT_ON_UPPER = 2;
    static int ALERT_ON_LOWER = 4;
       
    @DatabaseField(id = true, index = true,unique = true,canBeNull = false)
    protected String id;
    
    @DatabaseField
    protected DeviceType deviceType;
    
    @DatabaseField
    protected String name;
    
    @DatabaseField
    protected String cooker;
    
    @DatabaseField
    protected int targetTemp;

    @DatabaseField
    protected int lowerTempAlarm;

    @DatabaseField
    protected int upperTempAlarm;

    @DatabaseField
    protected AlarmType alarmType;

    @DatabaseField
    protected float currentTemp;
    
    @DatabaseField
    protected String blowerID;
    
    @DatabaseField
    protected String blowerName;
    
    @DatabaseField
    protected boolean blowerOn;
    
    @DatabaseField
    protected long   blowerRunTime;

    @DatabaseField
    protected long   alarmCondition;
    
    @DatabaseField
    protected Date   lastUpdateTime;
    
    public DeviceDAO() { }

    public DeviceDAO( DeviceDAO d )
    {
       this.alarmType = d.alarmType;;
       this.blowerID= d.blowerID;
       this.blowerName = d.blowerName;
       this.blowerOn = d.blowerOn;
       this.blowerRunTime = d.blowerRunTime;
       this.cooker = d.cooker;
       this.currentTemp = d.currentTemp;
       this.deviceType = d.deviceType;
       this.id = d.id;
       this.lowerTempAlarm = d.lowerTempAlarm;
       this.name = d.name;
       this.targetTemp = d.targetTemp;
       this.upperTempAlarm = d.upperTempAlarm;
       this.alarmCondition = d.alarmCondition;
       this.lastUpdateTime = d.lastUpdateTime;
    }
    
    public long getAlarmCondition()
    {
        return alarmCondition;
    }

    public void setAlarmCondition(long alarmCondition)
    {
        this.alarmCondition = alarmCondition;
    }

    public Date getLastUpdateTime()
    {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime)
    {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public DeviceType getDeviceType()
    {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType)
    {
        this.deviceType = deviceType;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCooker()
    {
        return cooker;
    }

    public void setCooker(String cooker)
    {
        this.cooker = cooker;
    }

    public int getTargetTemp()
    {
        return targetTemp;
    }

    public void setTargetTemp(int targetTemp)
    {
        this.targetTemp = targetTemp;
    }

    public int getLowerTempAlarm()
    {
        return lowerTempAlarm;
    }

    public void setLowerTempAlarm(int lowerTempAlarm)
    {
        this.lowerTempAlarm = lowerTempAlarm;
    }

    public int getUpperTempAlarm()
    {
        return upperTempAlarm;
    }

    public void setUpperTempAlarm(int upperTempAlarm)
    {
        this.upperTempAlarm = upperTempAlarm;
    }

    public AlarmType getAlarmType()
    {
        return alarmType;
    }

    public void setAlarmType(AlarmType alarmType)
    {
        this.alarmType = alarmType;
    }

    public float getCurrentTemp()
    {
        return currentTemp;
    }

    public void setCurrentTemp(float currentTemp)
    {
        this.currentTemp = currentTemp;
    }

    public String getBlowerID()
    {
        return blowerID;
    }

    public void setBlowerID(String blowerID)
    {
        this.blowerID = blowerID;
    }

    public String getBlowerName()
    {
        return blowerName;
    }

    public void setBlowerName(String blowerName)
    {
        this.blowerName = blowerName;
    }

    public boolean isBlowerOn()
    {
        return blowerOn;
    }

    public void setBlowerOn(boolean blowerOn)
    {
        this.blowerOn = blowerOn;
    }

    public long getBlowerRunTime()
    {
        return blowerRunTime;
    }

    public void setBlowerRunTime(long blowerRunTime)
    {
        this.blowerRunTime = blowerRunTime;
    }
    
    
}
