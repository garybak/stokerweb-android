package com.gbak.sweb.client.android.paid.data.dao;

import com.j256.ormlite.field.DatabaseField;


public class ProbeAlertStash extends ProbeAlert
{
    @DatabaseField( defaultValue = "false")
    private boolean updated;

    public ProbeAlertStash() { } 
    
    public ProbeAlertStash( ProbeAlert pa)
    {
        super(pa);
    }
    
    public boolean isUpated()
    {
        return updated;
    }

    public void setUpated(boolean upated)
    {
        this.updated = upated;
    }

}
