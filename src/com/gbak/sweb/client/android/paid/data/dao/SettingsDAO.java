package com.gbak.sweb.client.android.paid.data.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="settings")
public class SettingsDAO
{
    @DatabaseField(generatedId=true)
   private long id;
   
    @DatabaseField
    private String hostname;
    
    @DatabaseField
    private String port;
    
    @DatabaseField
    private String userName;
    
    @DatabaseField
    private String password;
    
    @DatabaseField
    private String baseURL;
    
    @DatabaseField
    private String serverConnectionType;
    
    @DatabaseField
    private boolean secureHTTP;
   
    @DatabaseField
    private String profileName;
    
    @DatabaseField
    private boolean active;
    
    @DatabaseField
    private String ringtoneURI;
    

 
    public SettingsDAO() { }
    
    public SettingsDAO( long id, String hostname, String port, String userName,
                     String password, String baseURL, String serverConnectionType, boolean https,
                     String profileName, boolean active, String ringtoneURI )
    {
        this.id = id;
        this.hostname = hostname;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.baseURL = baseURL;
        this.serverConnectionType = serverConnectionType;
        this.secureHTTP = https;
        this.profileName = profileName;
        this.active = active;
        this.ringtoneURI = ringtoneURI;
    }
    
    public String getServerConnectionType()
    {
        return serverConnectionType;
    }
    public void setServerConnectionType(String serverConnectionType)
    {
        this.serverConnectionType = serverConnectionType;
    }
    public long getId()
    {
        return id;
    }
    public void setId(long id)
    {
        this.id = id;
    }
    public String getHostname()
    {
        return hostname;
    }
    public void setHostname(String hostname)
    {
        this.hostname = hostname;
    }
    public String getPort()
    {
        return port;
    }
    public void setPort(String port)
    {
        this.port = port;
    }
    public String getUserName()
    {
        return userName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    public String getPassword()
    {
        return password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }
    public String getBaseURL()
    {
        return baseURL;
    }
    public void setBaseURL(String baseURL)
    {
        this.baseURL = baseURL;
    }

    public String getProfileName()
    {
        return profileName;
    }

    public void setProfileName(String profileName)
    {
        this.profileName = profileName;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
    public String getRingerURI()
    {
        return ringtoneURI;
    }

    public void setRingerURI(String ringerURI)
    {
        this.ringtoneURI = ringerURI;
    }

    public boolean isSecureHTTP()
    {
        return secureHTTP;
    }

    public void setSecureHTTP(boolean secureHTTP)
    {
        this.secureHTTP = secureHTTP;
    }
   
   
}

