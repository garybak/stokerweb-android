package com.gbak.sweb.client.android.paid.data;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.gbak.sweb.client.android.paid.data.dao.DeviceDAO;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlertStash;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;


import android.content.Context;
import android.database.Cursor;
import android.util.Log;


public class DatabaseManager 
{

    static private DatabaseManager instance;
    private final String TAG = "DatabaseManager";

    static public void init(Context ctx) 
    {
        if (null==instance) 
        {
            instance = new DatabaseManager(ctx);
        }
    }

    static public DatabaseManager getInstance() 
    {
        return instance;
    }

    private DatabaseHelper helper;
    private DatabaseManager(Context ctx) 
    {
        helper = new DatabaseHelper(ctx);
    }

    private DatabaseHelper getHelper() 
    {
        return helper;
    }

    public void setActiveSetting(String id)
    {
        
        try
        {
            UpdateBuilder<SettingsDAO, Integer> updateBuilder2 = getHelper().getSettingsDao().updateBuilder();
            updateBuilder2.updateColumnExpression("active", "id = " + id );
            updateBuilder2.update();
            getHelper().getSettingsDao().clearObjectCache();
     
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    public SettingsDAO getActiveSettings() throws NoActiveSettingException
    {
        List<SettingsDAO> settings = null;
        SettingsDAO s = null;
        
        try
        {
            settings = getHelper().getSettingsDao().queryForEq("active", true);
            if ( settings.size() > 0 )
                s = settings.get(0);
            else if ( settings.size() == 0 )
                throw new NoActiveSettingException();
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }

        return s;
    }
    public SettingsDAO getSettingsRow(String ID) 
    {
        SettingsDAO s = null;
        try 
        {
            s = getHelper().getSettingsDao().queryForId(Integer.valueOf(ID));
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }

        return s;
    }
    
    public void deleteSettings( String ID )
    {
        try
        {
            getHelper().getSettingsDao().deleteById(Integer.valueOf(ID));
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
    public List<SettingsDAO> getSettings() 
    {
        List<SettingsDAO> settings = null;
        try 
        {
            settings = getHelper().getSettingsDao().queryForAll();
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }

        return settings;
    }
    
    public List<ProbeAlert> getProbeAlarms() 
    {
        List<ProbeAlert> alarms = null;
        try 
        {
            alarms = getHelper().getProbeAlertDao().queryForAll();
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }

        return alarms;
    }
    
    public ProbeAlert getProbeAlarm(String probeID) 
    {
        List<ProbeAlert> alarms = null;
        ProbeAlert alarm = null;
        
        try 
        {
            alarms = getHelper().getProbeAlertDao().queryForEq("probeID", probeID);
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }

        if ( alarms.size() > 1 )
            Log.e("DatabaseManager","Multiple rows detected in ProbeAlert for probeID:" + probeID);
        
        if ( alarms.size() >= 1 )
            alarm = alarms.get(0); 
           
        return alarm; 
    }
    
    public void addProbeAlarm( ProbeAlert pa )
    {
        try
        {
            getHelper().getProbeAlertDao().create( pa );
        }
        catch( SQLException e )
        {
            e.printStackTrace();
        }
    }
    
    public void addSettings(SettingsDAO s) {
        try 
        {
            getHelper().getSettingsDao().create(s);
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }

    public void updateSettings(SettingsDAO settings) {
        try 
        {
            getHelper().getSettingsDao().update(settings);
        } 
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
    
    public DeviceDAO getDevice( String id )
    {
        List<DeviceDAO> list = null;
        DeviceDAO d = null;
        
        try
        {
           list = getHelper().getDeviceDao().queryForEq("id", id);  
           if ( list.size() > 0)
               d = list.get(0);
           if ( list.size() > 1)
               Log.e("DatabaseManager","More than 1 DeviceDAO fetch from DB, using key");
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return d;
    }
    
    public Date getOldestDeviceDate(String cookerName)
    {
        Date d = null;
        final QueryBuilder<DeviceDAO, Integer> queryBuilder = getHelper().getDeviceDao().queryBuilder();
        
        try
        {
            queryBuilder.orderBy("lastUpdateTime", true);
            if ( cookerName != null && cookerName.length() > 0)
                queryBuilder.where().eq("cooker", cookerName);
            queryBuilder.limit(1);
            List<DeviceDAO> listOfOne;
        
            listOfOne = queryBuilder.query();
            if ( listOfOne.size() > 0 )
                return listOfOne.get(0).getLastUpdateTime();
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
        
    }
    
    public List<DeviceDAO> getDevicesForCooker(String cookerName)
    {
        Date d = null;
        final QueryBuilder<DeviceDAO, Integer> queryBuilder = getHelper().getDeviceDao().queryBuilder();
        
        try
        {
            if ( cookerName != null && cookerName.length() > 0)
                queryBuilder.where().eq("cooker", cookerName);
            
            return queryBuilder.query();
            
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null; 
    }
    
    public CloseableIterator<DeviceDAO> getDeviceIterator()
    {
        CloseableIterator<DeviceDAO> iterator;
        try 
        {
            final QueryBuilder<DeviceDAO, Integer> queryBuilder = getHelper().getDeviceDao().queryBuilder();
            queryBuilder.orderBy("cooker", true);
            queryBuilder.groupBy("cooker");

            PreparedQuery<DeviceDAO> preparedQuery = queryBuilder.prepare();
          //  getHelper().getDeviceDao().query(preparedQuery);
            
           iterator = getHelper().getDeviceDao().iterator(preparedQuery);
          
        }
        catch (SQLException e) 
        {
            Log.e(TAG, "Error fetching devices with group: ", e);
            
            return null;
        }
        return iterator;
    }
    
    public List<DeviceDAO> getDevicesGrouped(String cookerName )
    {
        List<DeviceDAO> list = null;
        try 
        {
            final QueryBuilder<DeviceDAO, Integer> queryBuilder = getHelper().getDeviceDao().queryBuilder();
            queryBuilder.orderBy("cooker", true);
            
            if ( cookerName.length() > 0)
               queryBuilder.where().eq("cooker", cookerName );
            
         //   queryBuilder.groupBy("cooker");
         //   queryBuilder.selectColumns(fieldName);
          //  final Where<DeviceDAO, Integer> where = queryBuilder.where();
            /*where.eq(DeviceDAO, false).and();
            if (whereFields!=null){
                for (String key : whereFields.keySet()){
                    if (!whereFields.get(key).isEmpty()){
                        where.eq(key, whereFields.get(key)).and();
                    }
                }
            }*/
            PreparedQuery<DeviceDAO> preparedQuery = queryBuilder.prepare();
            list = getHelper().getDeviceDao().query(preparedQuery);
            
            /*final GenericRawResults<DeviceDAO> rawResults = getHelper().getDeviceDao().queryRaw(queryBuilder.prepareStatementString(), new RawRowMapper<DeviceDAO>() 
                    {
                        public DeviceDAO mapRow(String[] columnNames, String[] resultColumns) {
                            return resultColumns[0];
                       }

                    });
            List<DeviceDAO> results = rawResults.getResults();
            results.add(0, "");
            return results;*/
        }
        catch (SQLException e) 
        {
            Log.e(TAG, "Error fetching devices with group: ", e);
            
            return null;
        }
        
        return list;
    }
    public List<DeviceDAO> getDevice( )
    {
        List<DeviceDAO> list = null;
        
        try
        {
           list = getHelper().getDeviceDao().queryForAll();   
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return list;
    }
    
    public void clearDevice()
    {
        try
        {
            DeleteBuilder<DeviceDAO, Integer> deleteBuilder = getHelper().getDeviceDao().deleteBuilder();
            // Empty where clause should delete all rows;
            deleteBuilder.delete();
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
    
    
    
    
    
    public DeviceStashDAO getDeviceStash( String id )
    {
        List<DeviceStashDAO> list = null;
        DeviceStashDAO d = null;
        
        try
        {
           list = getHelper().getDeviceStashDao().queryForEq("id", id);  
           if ( list.size() > 0)
               d = list.get(0);
           if ( list.size() > 1)
               Log.e("DatabaseManager","More than 1 DeviceStashDAO fetch from DB, using key");
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return d;
    }

    
    public List<DeviceStashDAO> getDeviceStash( )
    {
        List<DeviceStashDAO> list = null;
        
        try
        {
           list = getHelper().getDeviceStashDao().queryForAll();   
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return list;
    }
    
    public void clearDeviceStash()
    {
        try
        {
            DeleteBuilder<DeviceStashDAO, Integer> deleteBuilder = getHelper().getDeviceStashDao().deleteBuilder();
            // Empty where clause should delete all rows;
            deleteBuilder.delete();
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
    
    public void updateDeviceStash( DeviceStashDAO device )
    {
        try
        {
            getHelper().getDeviceStashDao().createOrUpdate(device);
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
        
    }
    
    public void updateDevice( DeviceDAO device )
    {
        try
        {
            getHelper().getDeviceDao().createOrUpdate(device);
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
        
    }
    
    public void clearDevices()
    {
        try
        {
            DeleteBuilder<DeviceDAO, Integer> deleteBuilder = getHelper().getDeviceDao().deleteBuilder();
            deleteBuilder.delete();
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void updateDevices( List<DeviceDAO> devices )
    {
        try
        {
            for ( DeviceDAO d : devices )
               getHelper().getDeviceDao().createOrUpdate(d);
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
        
    }
    
    public List<ProbeAlert> getProbeAlert( )
    {
        List<ProbeAlert> list = null;
        
        try
        {
           list = getHelper().getProbeAlertDao().queryForAll();   
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return list;
    }
    
    public void updateProbeAlert( ProbeAlert pa )
    {
        try
        {
            getHelper().getProbeAlertDao().createOrUpdate(pa);
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
    }
    
    public List<ProbeAlertStash> getProbeAlertStash( )
    {
        List<ProbeAlertStash> list = null;
        
        try
        {
           list = getHelper().getProbeAlertStashDao().queryForAll();   
        }
        catch ( SQLException e )
        {
            e.printStackTrace();
        }
        
        return list;
    }
    
    public void clearProbeAlertStash()
    {
        try
        {
            DeleteBuilder<ProbeAlertStash, Integer> deleteBuilder = getHelper().getProbeAlertStashDao().deleteBuilder();
            // Empty where clause should delete all rows;
            deleteBuilder.delete();
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
    }
    
    public void updateProbeAlertStash( ProbeAlertStash device )
    {
        try
        {
            getHelper().getProbeAlertStashDao().createOrUpdate(device);
        }
        catch (SQLException e) 
        {
            e.printStackTrace();
        }
        
    }
    
}