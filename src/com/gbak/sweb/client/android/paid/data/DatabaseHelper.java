package com.gbak.sweb.client.android.paid.data;

import java.util.ArrayList;
import java.util.List;


import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.gbak.sweb.client.android.paid.data.dao.DeviceDAO;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlertStash;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper
{
    private static final String TAG = "DatabaseHelper";
    
    private static final String DATABASE_NAME = "stokerwebDB.sqlite";

    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 5;
    
    private static Context context;

    // the DAO object we use to access the SimpleData table
    private Dao<SettingsDAO, Integer> settingsDao = null;
    private Dao<ProbeAlert, Integer> probeAlertDao = null;
    private Dao<ProbeAlertStash, Integer> probeAlertStashDao = null;
    private Dao<DeviceDAO,Integer> deviceDao = null;
    private Dao<DeviceStashDAO,Integer> deviceStashDao = null;

    public DatabaseHelper(Context context) 
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        DatabaseHelper.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database,ConnectionSource connectionSource) 
    {
        try {
            TableUtils.createTable(connectionSource, SettingsDAO.class);
            TableUtils.createTable(connectionSource, ProbeAlert.class);
            TableUtils.createTable(connectionSource, ProbeAlertStash.class);
            TableUtils.createTable(connectionSource, DeviceStashDAO.class);
            TableUtils.createTable(connectionSource, DeviceDAO.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,ConnectionSource connectionSource, int oldVersion, int newVersion) 
    {
//        try {
//            Log.e("onUpgrade", "No database upgrade available");
//            /*List<String> allSql = new ArrayList<String>(); 
//            switch(oldVersion) 
//            {
//              case 1: 
//                  //allSql.add("alter table AdData add column `new_col` VARCHAR");
//                  //allSql.add("alter table AdData add column `new_col2` VARCHAR");
//            }
//            for (String sql : allSql) {
//                db.execSQL(sql);*/
//            }
//        } catch (SQLException e) {
//            Log.e(DatabaseHelper.class.getName(), "exception during onUpgrade", e);
//            throw new RuntimeException(e);
//        }
        Log.i(TAG, "onUpgrade, oldVersion=["+oldVersion+"], newVersion=["+newVersion+"]");
        try 
        {
            // Simply loop round until newest version has been reached and add the appropriate migration
            while (++oldVersion <= newVersion) 
            {
                switch (oldVersion) 
                {
                    // 3 was the first version
                    case 4: {
                        // Add migration for version 4 if required
                        UpgradeHelper.addUpgrade(4);
                        // Domain objects can still be created as normal with ORMLite
                    //    TableUtils.createTableIfNotExists(connectionSource, PlayerStats.class);
                        break;
                    }
                    case 5: {
                        // Add migration for version 4 if required
                        UpgradeHelper.addUpgrade(5);
                        // Domain objects can still be created as normal with ORMLite
                    //    TableUtils.createTableIfNotExists(connectionSource, PlayerStats.class);
                        break;
                    }
 
                   /* case 4: {
                        // Add migration for version 3 if required
                        UpgradeHelper.addUpgrade(3);
                        break;
                    }*/
                }
            }
            // Get all the available updates
            final List<String> availableUpdates = UpgradeHelper.availableUpdates(DatabaseHelper.context.getResources());
            Log.d(TAG, "Found a total of " + availableUpdates.size() +" update statements" );
     
            for (final String statement : availableUpdates) {
                db.beginTransaction();
                try {
                    Log.d(TAG, "Executing statement: " + statement);
                    db.execSQL(statement);
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
            }
        }
        catch (final SQLException e) {
            Log.e(TAG, "Can't migrate databases, bootstrap database, data will be lost" +  e);
            // Completely resource the database on failure, you would need to attempt to drop all existing tables in your onCreate method
            onCreate(db, connectionSource);
        }  
    }

    public Dao<SettingsDAO, Integer> getSettingsDao() 
    {
        if (null == settingsDao) {
            try {
                settingsDao = getDao(SettingsDAO.class);
                settingsDao.setObjectCache(true);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return settingsDao;
    }

    public Dao<ProbeAlert, Integer> getProbeAlertDao() 
    {
        if (null == probeAlertDao) {
            try {
                probeAlertDao = getDao(ProbeAlert.class);
                probeAlertDao.setObjectCache(true);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return probeAlertDao;
    }
    
    public Dao<DeviceDAO, Integer> getDeviceDao() 
    {
        if (null == deviceDao) {
            try {
                deviceDao = getDao(DeviceDAO.class);
                deviceDao.setObjectCache(true);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return deviceDao;
    }

    public Dao<DeviceStashDAO, Integer> getDeviceStashDao() 
    {
        if (null == deviceStashDao) {
            try {
                deviceStashDao = getDao(DeviceStashDAO.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return deviceStashDao;
    }

    public Dao<ProbeAlertStash, Integer> getProbeAlertStashDao() 
    {
        if (null == probeAlertStashDao) 
        {
            try 
            {
                probeAlertStashDao = getDao(ProbeAlertStash.class);
            }
            catch (java.sql.SQLException e) 
            {
                e.printStackTrace();
            }
        }
        return probeAlertStashDao;
    }

    
}
