package com.gbak.sweb.client.android.paid.data;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
 
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
 

/**
 * Used as a aid in the migration process for, loading required SQL files as specified by a given version
 * 
 * @author James Edward Morgan
 * https://gist.github.com/1354418/8cd2535e6718648e913007c4bab5c6be5c42195f
 */
public class UpgradeHelper {
 
    private static final String LOG_TAG = "UpgradeHelper";
 
    protected static final Set<Integer> VERSION;
    static {
        VERSION = new LinkedHashSet<Integer>();
    }
 
    /**
     * Add the given version to the list of available updates
     */
    public static final void addUpgrade(final int version) {
        Log.d(LOG_TAG, "Adding " + version + " to upgrade path");
        VERSION.add(version);
    }
 
    /**
     * Get all available SQL Statements
     * 
     * @param resources the {@link Resources} from the given {@link Context} which maybe using the helper class
     * @return A list of SQL statements which have been included in the
     */
    public static List<String> availableUpdates(final Resources resources) {
        final List<String> updates = new ArrayList<String>();
 
        for (final Integer version : VERSION) {
                // Migration files must be kept in assets/updates/migration-X.sql
            final String fileName = String.format("updates/migration-%s.sql", version);
 
            Log.d(LOG_TAG, "Adding db version [" + version + "] to update list, loading file ["+ fileName +"]" );
 
            final String sqlStatements = loadAssetFile(resources, fileName);
 
            final String[] splitSql = sqlStatements.split("\\r?\\n");
            for (final String sql : splitSql) {
                if (isNotComment(sql)) {
                    updates.add(sql);
                }
            }
        }
        return updates;
    }
 
    /**
     * Load the given asset file, throws wrapped {@link RuntimeException} if not found
     * 
     * @param fileName of the file to load, including asset directory path and sub path if required
     * @param resources the {@link Resources}, usually from a {@link Context}
     * @return the fully loaded file as a {@link String}
     */
    private static String loadAssetFile(final Resources resources, final String fileName) {
        try {
            final InputStream is = resources.getAssets().open(fileName);
            final byte[] buffer = new byte[is.available()];
            is.read(buffer);
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            os.write(buffer);
            os.close();
            is.close();
            return os.toString();
        }
        catch (final IOException e) {
            Log.e(LOG_TAG, "IOException: " + e);
            throw new RuntimeException(e);
        }
    }
 
    /**
     * A comment must consist of either '--' or '#'
     * 
     * @return true if not found to be an SQL Comment
     */
    private static boolean isNotComment(final String sql) {
        //return !StringUtils.startsWith(sql, "--") || !StringUtils.startsWith(sql, "#");
        if ( sql == null )
            return true;
        
        return !sql.startsWith("--") || !sql.startsWith("#");
    }
}
