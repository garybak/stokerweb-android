package com.gbak.sweb.client.android.paid.data.dao;

import java.util.Calendar;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class ProbeAlert
{
  //  @DatabaseField(generatedId=true)
   // private long id;
       
    @DatabaseField(id = true)
    private String probeID;
    
    @DatabaseField
    private boolean enabled;
    
    @DatabaseField
    private boolean ignoreTimerEnabled;
    
    @DatabaseField
    private Date ignoreTimerReEnableTime;
    
    public ProbeAlert() { }
    
    public ProbeAlert( ProbeAlert pa )
    {
       // this.id = pa.id;
        this.probeID = pa.probeID;
        this.enabled = pa.enabled;
        this.ignoreTimerEnabled = pa.ignoreTimerEnabled;
        this.ignoreTimerReEnableTime = pa.ignoreTimerReEnableTime;
    }
    
    public ProbeAlert(/* long id, */ String probeID, boolean enabled, 
                        boolean ignoreTimeEnabled, Date ignoreTimerReEnableTime )
    {
        //this.id = id;
        this.probeID = probeID;
        this.enabled = enabled;
        this.ignoreTimerEnabled = ignoreTimeEnabled;
        this.ignoreTimerReEnableTime = ignoreTimerReEnableTime;
    }

    public void setMinutes( int minutes)
    {
        enabled = true;
        ignoreTimerEnabled = true;
        Date d = Calendar.getInstance().getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.add( Calendar.MINUTE, minutes );
        ignoreTimerReEnableTime = c.getTime();
    }
    /*
    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }
*/
    public String getProbeID()
    {
        return probeID;
    }

    public void setProbeID(String probeID)
    {
        this.probeID = probeID;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isIgnoreTimerEnabled()
    {
        return ignoreTimerEnabled;
    }

    public void setIgnoreTimerEnabled(boolean ignoreTimerEnabled)
    {
        this.ignoreTimerEnabled = ignoreTimerEnabled;
    }

    public Date getIgnoreTimerReEnableTime()
    {
        return ignoreTimerReEnableTime;
    }

    public void setIgnoreTimerReEnableTime(Date ignoreTimerReEnableTime)
    {
        this.ignoreTimerReEnableTime = ignoreTimerReEnableTime;
    }
    
    
}
