package com.gbak.sweb.client.android.paid.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.gbak.sweb.client.android.paid.convert.DAOConversionUtil;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlertStash;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.DeviceDataList;

public class SaveDataProcessor
{

    
    public static DeviceDataList saveConfiguration()
    {
        List<DeviceStashDAO> daoList =  DatabaseManager.getInstance().getDeviceStash();
        ArrayList<Device> deviceList = new ArrayList<Device>();
        boolean stokerSave = false;
        
        DeviceDataList ddl = new DeviceDataList();
        
        for ( DeviceStashDAO dao : daoList )
        {
            if ( dao.isUpated() )
                stokerSave = true;
            
            Device d = DAOConversionUtil.convertDeviceDAOToDevice( dao );
            deviceList.add( d );
        }
        
        if ( stokerSave == false)
            deviceList.clear();
        
        // Pull from alert Stash and move to regular alert table
        for ( ProbeAlertStash pas : DatabaseManager.getInstance().getProbeAlertStash())
        {
           ProbeAlert pa = pas;
           DatabaseManager.getInstance().updateProbeAlert(pa );
        }
        
        ddl.devices = deviceList;
        ddl.receivedDate = Calendar.getInstance().getTime();
        
       // clearStashes();
        return ddl;
    }

    
    public static void clearStashes()
    {
        DatabaseManager.getInstance().clearDeviceStash();
        DatabaseManager.getInstance().clearProbeAlertStash();
    }
}
