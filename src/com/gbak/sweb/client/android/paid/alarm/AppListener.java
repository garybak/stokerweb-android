
package com.gbak.sweb.client.android.paid.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.SystemClock;
import com.commonsware.cwac.wakeful.WakefulIntentService;

public class AppListener implements WakefulIntentService.AlarmListener 
{
  public void scheduleAlarms(AlarmManager mgr, PendingIntent pi,
                             Context ctxt) 
  {
    mgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime()+60000, 120000, pi );
                            //AlarmManager.INTERVAL_FIFTEEN_MINUTES, pi);
  }

  public void sendWakefulWork(Context ctxt) 
  {
    WakefulIntentService.sendWakefulWork(ctxt, AppService.class);
  }

  public long getMaxAge() 
  {
    return(AlarmManager.INTERVAL_FIFTEEN_MINUTES*2);
  }
}
