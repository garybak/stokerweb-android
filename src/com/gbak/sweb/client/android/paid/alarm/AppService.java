package com.gbak.sweb.client.android.paid.alarm;

import java.util.ArrayList;

import com.gbak.sweb.client.android.paid.MainActivity;

import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.service.FetchDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.json.Alert;
import com.gbak.sweb.common.json.DeviceDataList;

import com.gbak.sweb.client.android.paid.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;


import com.commonsware.cwac.wakeful.WakefulIntentService;

public class AppService extends WakefulIntentService
{
    NotificationManager mNM;
    final String TAG = "AppService";

    public AppService()
    {
        super("AppService");
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        // mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy()
    {
        // Cancel the persistent notification.
      //  mNM.cancel(1);

        // Tell the user we stopped.
      //  Toast.makeText(this, "Service stopped", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void doWakefulWork(Intent intent)
    {

        Log.i(TAG, "Wakeful alert...");
       // StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;

//        // No settings information or connection.
//        if ( swData == null)
//        {
//            Log.i(TAG,"DataService is null, can't do anything");
//            return;
//        }
        
        DeviceDataList deviceDataList = null;
        try
        {
          // deviceDataList = swData.getTempData();
          // SWUtil.checkAlarmsAndAlert( deviceDataList );
            deviceDataList = FetchDataService.getData(true);

           showNotification(SWUtil.checkAlarmsAndAlert( deviceDataList ));
        }
        catch ( UnableToConnectException uce )
        {
            Log.e(TAG,"Unable to make connection to get data for alert");
        }
        catch (NoActiveSettingException e)
        {
            Log.e(TAG,"No Active Setting to retrieve data");
            
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private void showNotification(ArrayList<Alert> alerts )
    {

       
        for ( Alert alert : alerts )
        {
            NotificationManager mNotificationManager;
            mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            
            Notification notifyDetails = new Notification(R.drawable.status_egg_icon,"Alarm on probe: " + alert.name,System.currentTimeMillis());
            
            Context context = getApplicationContext();
            CharSequence contentTitle = "Alarm: " + alert.name;
            CharSequence contentText = alert.message;
            Intent notifyIntent = new Intent(this, MainActivity.class);
            notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(this,  0,  notifyIntent , 0 );
            notifyDetails.flags |=  Notification.FLAG_AUTO_CANCEL;
            notifyDetails.defaults |= Notification.DEFAULT_ALL;
            notifyDetails.setLatestEventInfo(context, contentTitle, contentText, intent);
           // Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.status_egg);
           // notifyDetails.largeIcon = icon;
     
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.status_egg_icon)
                    .setContentTitle("Alarm: " + alert.name)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .setContentText(alert.message);
                // Creates an explicit intent for an Activity in your app
              //  Intent resultIntent = new Intent(this, MainActivity.class);
                // The stack builder object will contain an artificial back stack for the
                // started Activity.
                // This ensures that navigating backward from the Activity leads out of
                // your application to the Home screen.
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                // Adds the back stack for the Intent (but not the Intent itself)
                stackBuilder.addParentStack(MainActivity.class);
                // Adds the Intent that starts the Activity to the top of the stack
                stackBuilder.addNextIntent(notifyIntent);
                PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    );
                mBuilder.setContentIntent(resultPendingIntent);

                // to set custom sound
                
                try
                {
                    mBuilder.setSound(Uri.parse(DatabaseManager.getInstance().getActiveSettings().getRingerURI()));
                }
                catch (NoActiveSettingException e)
                {
                    e.printStackTrace();
                }

            
                mNotificationManager.notify(123, mBuilder.build());
            
        //    mNotificationManager.notify(123, notifyDetails);

            break;
        }
      
    }

}
