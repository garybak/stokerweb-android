package com.gbak.sweb.client.android.paid.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;
import com.gbak.sweb.client.android.paid.convert.DAOConversionUtil;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.constants.ServerConnectionType;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.StokerWebDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;
import android.util.Log;

public class FetchDataService
{
    public static Date lastRetreivedDate = null;

    public static String TAG = "FetchDataService";
    FetchDataService()
    {
        
    }
    
    public static void clearLastRetreivedDate()
    {
        lastRetreivedDate = null;
    }
    
    public static boolean updateRequired(String cookerName) throws NoActiveSettingException
    {
        if ( cookerName == null)
            cookerName = "";
        
        SettingsDAO s = DatabaseManager.getInstance().getActiveSettings();
        
        int updateSeconds = 120;
        
        if ( lastRetreivedDate == null )
        {
            Log.d(TAG,"lastRetreivedDate is null");
            return true;
        }
        
        if ( s.getServerConnectionType().compareTo(ServerConnectionType.STOKER.toString()) != 0 )
        {
          //  Log.d(TAG,"ConnectionType is StokerWeb");
            return true;    
        }
 
        Calendar now = Calendar.getInstance();
        Date d = DatabaseManager.getInstance().getOldestDeviceDate( cookerName );
        if ( d == null)
        {
            Log.d(TAG,"Oldest Device Date is null");
            return true;
        }
       // now.setTime( d );
       // now.add(Calendar.SECOND, updateSeconds);  // TODO: configurable
        
       // Log.d(TAG,"before now.getTime()" + now.getTime());
       // Log.d(TAG,"before time now: " + lastRetreivedDate);
        if ( d.after( lastRetreivedDate) )
        {
          //  Log.d(TAG,"now.getTime()" + now.getTime());
          //  Log.d(TAG,"time now: " + lastRetreivedDate);
            //return true;
            lastRetreivedDate = now.getTime();
           return true;
        }
        
      //  Log.d(TAG,"returning false");
        return false;
    }
    
    public static LogItemList getLogData(boolean forceLive, String cookerName) throws UnableToConnectException, NoActiveSettingException
    {
        LogItemList logItemList = new LogItemList();
        boolean liveData = false;
        
        //DataStatus ds = hasUpdatedData();
        
        if ( forceLive == true || updateRequired("") )
        {
            // fetch new data
            StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;

            lastRetreivedDate = Calendar.getInstance().getTime();
            logItemList = swData.getLogData(cookerName);
        //       DatabaseManager.getInstance().clearDevices();
               DatabaseManager.getInstance().updateDevices(DAOConversionUtil.convertToDeviceDaoList(logItemList));
               liveData = true;
               
               Log.d(TAG,"Pulling live Log data");

        }
       /* if ( liveData == false )
        {
            // Use database
            logItemList = DAOConversionUtil.convertToLogItemList( DatabaseManager.getInstance().getDevicesGrouped(cookerName));
            Log.d(TAG,"Pulling database Log data");
            
        }*/
        
        return logItemList;
    }
    
    public static DeviceDataList getData(boolean forceLive) throws UnableToConnectException, NoActiveSettingException
    {
        
        DeviceDataList deviceDataList = null;
        boolean liveData = false;
        
        //DataStatus ds = hasUpdatedData();
        
        if ( forceLive == true || updateRequired("") )
        {
            // fetch new data
            StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;

            if ( swData != null )
            {
               lastRetreivedDate = Calendar.getInstance().getTime();
               deviceDataList = swData.getTempData();
               DatabaseManager.getInstance().clearDevices();
               DatabaseManager.getInstance().updateDevices(DAOConversionUtil.convertToDeviceDaoList(deviceDataList));
               liveData = true;
               deviceDataList = SWUtil.addCookerBlanks(deviceDataList);
               Log.d(TAG,"Pulling live data");
            }

        }
        if ( liveData == false )
        {
            // Use database
            deviceDataList = DAOConversionUtil.convertToDataList( DatabaseManager.getInstance().getDevicesGrouped(""));
            Log.d(TAG,"Pulling database data");
            
        }

        return deviceDataList;
    }
}
