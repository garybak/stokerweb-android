package com.gbak.sweb.client.android.paid.service;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;
import com.gbak.sweb.client.android.paid.data.SaveDataProcessor;
import com.gbak.sweb.client.android.paid.services.StokerWebDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.common.json.DeviceDataList;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class SaveDataService extends Service
{
    private static final String TAG = "SaveDataService";

    private static final String ACTIVITY_RECEIVER_SAVE_START = "SaveStart";
    private static final String ACTIVITY_RECEIVER_SAVE_END = "SaveEnd";

    
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        Log.d(TAG, "onCreate");
        super.onCreate();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onStart(Intent intent, int startid)
    {
        Log.d(TAG, "onStart");

        new SaveSettingsTask().execute();
    }

    private void notifyStartSave()
    {
        Intent new_intent = new Intent();
        new_intent.setAction(ACTIVITY_RECEIVER_SAVE_START);
        sendBroadcast(new_intent);
    }
    
    private void notifyEndSave()
    {
        Intent new_intent = new Intent();
        new_intent.setAction(ACTIVITY_RECEIVER_SAVE_END);
        sendBroadcast(new_intent);
    }
    
    private class SaveSettingsTask extends AsyncTask<Void, Void, Boolean>
    {

        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
            {
                Toast.makeText(StokerWebAndroid.getContext(),
                        "Save Successful", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(StokerWebAndroid.getContext(), "Save Failed",
                        Toast.LENGTH_SHORT).show();
            }
            notifyEndSave();
        }

        @Override
        protected Boolean doInBackground(Void... arg0)
        {
            try
            {
                notifyStartSave();
                StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;
                DeviceDataList ddl = SaveDataProcessor.saveConfiguration();
                
                // Don't call the server is none of the server data has changed.
                if ( ddl.devices.size() > 0 )
                   swData.saveConfiguration(ddl);
            }
            catch (UnableToConnectException uce)
            {
                Log.e("DeviceSaveTask",
                        "Unable to make connection to save configuration");
                notifyEndSave();
                return false;
            }
            catch (NoActiveSettingException e)
            {
                Log.e("DeviceSaveTask",
                        "No Active setting to save data");
                e.printStackTrace();
                notifyEndSave();
            }
            return true;
        }
    }

}