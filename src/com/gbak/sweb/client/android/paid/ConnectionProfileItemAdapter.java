package com.gbak.sweb.client.android.paid;

import java.util.ArrayList;
import java.util.List;

import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class ConnectionProfileItemAdapter extends ArrayAdapter<SettingsDAO>
{
    private final String TAG = "ConnectionProfileItemAdapter";
    private Context context;
    private LayoutInflater m_Inflater;
    private List<SettingsDAO> m_settings = null;
    
    public ConnectionProfileItemAdapter(Context context, int resource, List<SettingsDAO> objects)
    {
        super(context, resource,  objects);
        
        this.context = context;
        
        m_settings = objects;
        m_Inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        ViewHolder holder = null;
        SettingsDAO s = m_settings.get(position);
        
        if ( convertView == null)
            v = m_Inflater.inflate(R.layout.connection_profile_item, parent, false);
        
        int resid = 0;
        if ( s.isActive())
        {
            resid = R.drawable.active_border;
            //v.setBackgroundResource(R.drawable.active_border);
            
        }
        v.setBackgroundResource(resid);
        
        TextView profileName = (TextView) v.findViewById(R.id.text_ProfileName);
        
        TextView hostname = (TextView) v.findViewById(R.id.profilesTextHostName);
        TextView port = ( TextView ) v.findViewById(R.id.profilesTextPort);
        TextView userName = ( TextView ) v.findViewById(R.id.profilesTextUser);
        TextView baseURL = ( TextView ) v.findViewById(R.id.profilesTextBaseURL);
        
        //CheckBox httpsCheckBox = (CheckBox) v.findViewById(R.id.profilesCheckBoxHTTPS);
        TextView httpsText = ( TextView ) v.findViewById( R.id.profilesTextHTTPS );
        //httpsCheckBox.setClickable(false);
        TextView stokerMode = ( TextView ) v.findViewById(R.id.profilesTextStokerOption);
        
        
        profileName.setText(s.getProfileName());
        hostname.setText(s.getHostname());
        port.setText(s.getPort());
        userName.setText(s.getUserName());
        baseURL.setText(s.getBaseURL());
        
        // httpsCheckBox.setChecked(s.isSecureHTTP());
        int text = 0;
        if ( s.isSecureHTTP() )
        
            text = R.string.profiles_label_https_yes;
        else
            text = R.string.profiles_label_https_no;
        
        httpsText.setText(context.getString(text));
        
        stokerMode.setText( s.getServerConnectionType());
        
       // Log.d(TAG, "position: " + position );
        
        
        return v;
        
    }
    
    public void update(List<SettingsDAO> objects )
    {
        m_settings = objects;
    }
    
    public int getCount()
    {
        if ( m_settings == null )
            return 0;
        
        return m_settings.size();
    }
    
    public static class ViewHolder {
        public TextView textView;
    }

}
