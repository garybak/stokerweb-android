package com.gbak.sweb.client.android.paid;

import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ProbeDetailPageAdapter extends FragmentPagerAdapter
{

    private List<Fragment> fragments;
    
    public ProbeDetailPageAdapter(FragmentManager fm) {  
        super(fm);  
   }  
    
    public ProbeDetailPageAdapter(FragmentManager fm, List<Fragment> fragments)
    {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position)
    {
        return this.fragments.get(position);

    }

    @Override
    public int getCount()
    {
        return this.fragments.size();
    }

    
    
}
