package com.gbak.sweb.client.android.paid;

import java.util.ArrayList;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


import com.gbak.sweb.client.android.paid.R;


import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;


import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ProbeDetailPager extends FragmentActivity
{
    private static String TAG = "ProbeDetailPager";
    
    private ProbeDetailPageAdapter pagerAdapter;
    private ViewPager viewPager;
    
    private Button buttonSave;
    private Button buttonCancel;
    boolean save = false;
    private FragmentManager fragManager;
    List<Fragment> fragmentList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.outer_probe_detail);
        
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        fragmentList = new ArrayList<Fragment>();

        ArrayList<String> deviceList = getIntent().getStringArrayListExtra(
                "deviceList");
        String currentID = getIntent().getExtras().getString("selectedID");
        int currentItem = 0;
        int count = 0;
        
         for (String s : deviceList) 
        {
            Fragment f = Fragment.instantiate(this, ProbeDetailFragment.class.getName());

            Log.d("ProbeDetailPager","fragment ID: " + f.getId() );
            getIntent().putExtra("id", s);  // This tell the fragment which probe to display
            f.setArguments(getIntent().getExtras());
            fragmentList.add(f);
            if (s.compareTo(currentID) == 0)
            {
                currentItem = count;
            }
            count++;
            
        }

        pagerAdapter = new ProbeDetailPageAdapter(super.getSupportFragmentManager(), fragmentList);
        fragManager = super.getSupportFragmentManager();
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(currentItem);
        Log.d("ProbeDetailPager","setCurrent: " + currentItem );
      

        setupButtons();
    }
 
    protected void setupButtons()
    {
        
        buttonSave = (Button) findViewById(R.id.probe_detail_button_save);
        buttonSave.setOnClickListener( saveHandler );
        
        buttonCancel = (Button) findViewById(R.id.probe_detail_button_cancel);
        buttonCancel.setOnClickListener( cancelHandler );
    }
    
    private OnClickListener saveHandler = new OnClickListener()
    {
        public void onClick(View v)
        {
           // saveSettings();
            
         //   int CurrentItem = viewPager.getCurrentItem();
          //  String name = getTag(CurrentItem);
           // fragment = getSupportFragmentManager().findFragmentByTag(name);
            
         //   Fragment fragment = fragManager.findFragmentById(CurrentItem);

            
            // TODO: Update ProbeAlarm table and Stoker with stash data
            
          //  clearAll();
            for ( Fragment f : fragmentList )
            {
                ProbeDetailFragment pdf = (ProbeDetailFragment) f;
                pdf.saveSettings();
            }
            
            saveData();
            save = true;
            finish();
            
        }
    };
    
    @Override
    protected void onStop()
    {
      super.onStop();
      
      // Save all data here
      Log.d("ProbeDetailPager","saving? : " + save );
      
      // background update
    //  if ( save == true )
    //      saveData();

    }
    
    private OnClickListener cancelHandler = new OnClickListener()
    {
        public void onClick(View v) 
        {
            save = false;
            finish();
        }
    };
    
    public void onBackPressed()
    {
        save=false;
        finish();
    }
    
    private void saveData()
    {
        Intent serviceIntent = new Intent(StokerWebAndroid.getContext(),com.gbak.sweb.client.android.paid.service.SaveDataService.class);
        //serviceIntent.setAction("com.gbak.sweb.client.android.paid.service.SaveDataService");
        StokerWebAndroid.getContext().startService(serviceIntent);
    }
    
}
