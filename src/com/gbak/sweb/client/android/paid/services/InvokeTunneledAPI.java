package com.gbak.sweb.client.android.paid.services;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.ObjectMapper;

@JsonPropertyOrder({ "appName", "methodID", "methodParameters" })
public class InvokeTunneledAPI {
	
	ObjectMapper m_jacksonObjectMapper = new ObjectMapper();


	@JsonProperty(value="appName")
	private String appName;
	
	@JsonProperty(value="methodID")
	private String methodId;
	
	@JsonProperty(value="methodParameters")
	private String methodParameters;
	
	public InvokeTunneledAPI(){}

	public InvokeTunneledAPI(String json) throws Exception
	{
		InvokeTunneledAPI temp = m_jacksonObjectMapper.readValue(json, InvokeTunneledAPI.class);	

		this.appName = temp.appName;
		this.methodId = temp.methodId;
		this.methodParameters = temp.methodParameters;
	}
	
	public String toJsonString() throws Exception
	{    
		return m_jacksonObjectMapper.writeValueAsString(this);
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppName() {
		return appName;
	}

	public void setMethodId(String methodId) {
		this.methodId = methodId;
	}

	public String getMethodId() {
		return methodId;
	}

	public void setMethodParameters(String methodParameters) {
		this.methodParameters = methodParameters;
	}

	public String getMethodParameters() {
		return methodParameters;
	}
}
