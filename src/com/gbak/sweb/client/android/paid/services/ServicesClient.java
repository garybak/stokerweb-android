package com.gbak.sweb.client.android.paid.services;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;

import android.util.Log;


public class ServicesClient  {
    
    private DefaultHttpClient defaultHttpClient;
    private static final String TAG = "ServiceClient";
    
    public ServicesClient() 
    {
        KeyStore trustStore = null;
        
        
        try
        {
            trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            final SSLSocketFactory sf = new MockSSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            
            defaultHttpClient = new DefaultHttpClient()
            {
                
                @Override
                final protected ClientConnectionManager createClientConnectionManager() {
                SchemeRegistry registry = new SchemeRegistry();
                registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                registry.register(new Scheme("https", sf, 443));
                
                return new SingleClientConnManager(getParams(), registry);
            }};
            
            // Set timeout                
            int timeoutConnection = Integer.valueOf(StokerWebAndroid.getPropertiesManager().getProperty("ConnectionTimeout")) * 1000;
         
            final HttpParams httpParams = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
            HttpConnectionParams.setSoTimeout        (httpParams, timeoutConnection);
        
        }
        catch (KeyManagementException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (UnrecoverableKeyException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (KeyStoreException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (CertificateException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    
    public String executePost(String address, String request) throws ClientProtocolException, IOException
    {
        HttpPost post = new HttpPost(address);
        StringEntity input = new StringEntity(request);
        input.setContentType("application/json");
        post.setEntity(input);
        
        int timeoutConnection = Integer.valueOf(StokerWebAndroid.getPropertiesManager().getProperty("ConnectionPostTimeout")) * 1000;
        final HttpParams httpParams = defaultHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
        HttpConnectionParams.setSoTimeout        (httpParams, timeoutConnection);
        
        HttpResponse response = defaultHttpClient.execute(post);

          // Check if server response is valid
          StatusLine status = response.getStatusLine();
          
          // Pull content stream from response
          HttpEntity entity = response.getEntity();
          InputStream inputStream = entity.getContent();
        
          ByteArrayOutputStream content = new ByteArrayOutputStream();
        
          // Read response into a buffered stream
          int readBytes = 0;
          byte[] sBuffer = new byte[512];
          while ((readBytes = inputStream.read(sBuffer)) != -1) {
              content.write(sBuffer, 0, readBytes);
          }
        
          // Return result from buffered stream
          String dataAsString = new String(content.toByteArray());
        
          if (status.getStatusCode() != 200) {
              throw new IOException("Invalid response from server: " + status.toString());
          }
          
        return dataAsString;
    }
    
    public String executeGet(String address) throws ClientProtocolException, IOException
    {
        synchronized(this)
        {

            HttpGet get = new HttpGet(address);
            StringEntity input = new StringEntity("");
            input.setContentType("application/json");
          //  get.setEntity(input);
            
            HttpResponse response = defaultHttpClient.execute(get);

          // Check if server response is valid
          StatusLine status = response.getStatusLine();
          
          String dataAsString = EntityUtils.toString(response.getEntity()); 
        
          if (status.getStatusCode() != 200) {
              throw new IOException("Server Response: " + status.toString());
          }
          
          response.getEntity().consumeContent();
        
        return dataAsString;
        }
    }
    
    public String executePut(String address, String request) throws ClientProtocolException, IOException
    {
        synchronized(this)
        {

            HttpPut put = new HttpPut(address);
            StringEntity input = new StringEntity(request);
            input.setContentType("application/json");
            put.setEntity(input);
            
            HttpResponse response = defaultHttpClient.execute(put);

          // Check if server response is valid
          StatusLine status = response.getStatusLine();
          
          String dataAsString = EntityUtils.toString(response.getEntity()); 
        
          if (status.getStatusCode() != 200 && status.getStatusCode() != 201 ) 
          {
              Log.e(TAG,"Server returned invalid response code: " + status.getStatusCode());
              throw new IOException("Server Response: " + status.toString());
          }
          
          response.getEntity().consumeContent();
        
        return dataAsString;
        }
    }
    
    public String executeDelete(String address) throws ClientProtocolException, IOException
    {
        synchronized(this)
        {

            HttpDelete delete = new HttpDelete(address);
            //StringEntity input = new StringEntity("");
           // input.setContentType("application/json");
           // delete.setEntity(input);
            
            HttpResponse response = defaultHttpClient.execute(delete);

          // Check if server response is valid
          StatusLine status = response.getStatusLine();
          
          String dataAsString = EntityUtils.toString(response.getEntity()); 
        
          if (status.getStatusCode() != 200) 
          {
              Log.e(TAG,"Server returned invalid response code: " + status.getStatusCode());
              throw new IOException("Server Response: " + status.toString());
          }
          
          response.getEntity().consumeContent();
        
        return dataAsString;
        }
    }

}

