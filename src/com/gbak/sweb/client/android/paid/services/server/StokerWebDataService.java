package com.gbak.sweb.client.android.paid.services.server;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import android.util.Log;
import android.widget.Toast;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.constants.ServerConnectionType;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.ServicesClient;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.base.JacksonObjectMapper;
import com.gbak.sweb.common.base.RequestBase;
import com.gbak.sweb.common.base.ServiceResult;
import com.gbak.sweb.common.json.CookerList;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;
import com.gbak.sweb.common.json.LogNote;
import com.gbak.sweb.common.json.LoginData;
import com.gbak.sweb.common.json.ServerRequest;

public class StokerWebDataService implements com.gbak.sweb.client.android.paid.services.StokerWebDataService
{

    private static String TAG = "StokerWebDataService";
    DeviceDataList deviceData;
    CookerList cookerList;
    
    private static final String LogDataPath = "/logs/cooker";
    
    public String  getAddress(String serviceContext, SettingsDAO s) throws NoActiveSettingException
    {
        String address;
        synchronized( this )
        {
            SettingsDAO settings = null;
            if ( s == null )
               settings = DatabaseManager.getInstance().getActiveSettings();
            else
                settings = s;
            
            String httpType = "http://";
            
            if ( settings.getServerConnectionType().compareTo(ServerConnectionType.STOKER_WEB.toString()) == 0
                    && settings.isSecureHTTP())
                httpType = "https://";
            address = httpType + settings.getHostname() + ":" + settings.getPort();
        
             String baseURL = "";
            if ( settings.getBaseURL() != null)
                if ( settings.getBaseURL().length() > 0)
                    baseURL = "/" + settings.getBaseURL();
            
            
            address =  address + baseURL + "/api/v1" + serviceContext;

            Log.i(TAG,"Built stokerweb address: " + address);
        }
        return address;
    }
    
    public String getTestAddress(SettingsDAO s)
    {
        String address = null;
        try
        {
            address = getAddress("/devices", s);
        }
        catch (NoActiveSettingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return address;
        
    }
    public LogItemList getLogData(String cookerName ) throws UnableToConnectException
    {
        boolean result = false;
        LogItemList logItemList = null;
        
        try
        {
            String encodedCookerName = "";
            if ( cookerName != null && cookerName.length() > 0 )
               encodedCookerName = "/" + URLEncoder.encode(cookerName, "UTF-8").replace("+" , "%20");
            
            String address = getAddress(LogDataPath, null) + encodedCookerName;
            
            String response = StokerWebAndroid.getServicesClient().executeGet(address);

            TypeReference<LogItemList> typeRef = new TypeReference<LogItemList>() { };
            
            logItemList = JacksonObjectMapper.INSTANCE.mapper.readValue(response, typeRef);
            
            if (deviceData != null)
            {
                result = true;
            }
            else
            {
                Log.d("StokerWebDataSerivce", "Call to server failed");
            }
 
        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }
        
        return logItemList;
    }
    
    public DeviceDataList getTempData() throws UnableToConnectException
    {
        boolean result = false;
        try
        {
           // RequestBase request = new RequestBase();
            String address = getAddress("/devices", null);
            String response = StokerWebAndroid.getServicesClient().executeGet(address);

            TypeReference<DeviceDataList> typeRef = new TypeReference<DeviceDataList>() { };
            
            deviceData = JacksonObjectMapper.INSTANCE.mapper.readValue(response, typeRef);
            
            if (deviceData != null)
            {
                result = true;
            }
            else
            {
                Log.d("StokerWebDataSerivce", "Call to server failed");
            }
            

        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }

       // return SWUtil.addCookerBlanks(deviceData);
        return deviceData;
    }

    public CookerList getCookerList() throws UnableToConnectException
    {
        
        boolean result = false;
        try
        {
           // RequestBase request = new RequestBase();
            String address = getAddress("/cookers", null);
            String response = StokerWebAndroid.getServicesClient().executeGet( address);

            TypeReference<CookerList> typeRef = new TypeReference<CookerList>() {
            //TypeReference<ServiceResult<DeviceDataList>> typeRef = new TypeReference<ServiceResult<DeviceDataList>>() {
            };
            
            deviceData = JacksonObjectMapper.INSTANCE.mapper.readValue(response, typeRef);
            
            if (deviceData != null)
            {
                result = true;
           //     deviceData = serviceResult.get1stDataEntry();
            }
            else
            {
                Log.d("CookerList", "Call to server failed");
            }

        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }


        return cookerList;
    }


    public void newNote(LogNote ln) throws UnableToConnectException
    {
        boolean result = false;
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = "";
            
            SettingsDAO s = DatabaseManager.getInstance().getActiveSettings();
            LoginData loginData = new LoginData(s.getUserName(), s.getPassword());
            ServerRequest<LogNote> serverRequest = new ServerRequest<LogNote>();
            serverRequest.data = ln;
            serverRequest.login = loginData;
            
            jsonString = mapper.writeValueAsString(serverRequest);
            
           // RequestBase request = new RequestBase();
            String address = getAddress("/logs/note", null);
            String response = new ServicesClient().executePut( address, jsonString);

            result = true;


        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }
    }
    
    public void newLog(LogItem item) throws UnableToConnectException
    {
        boolean result = false;
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = "";
            
            SettingsDAO s = DatabaseManager.getInstance().getActiveSettings();
            LoginData loginData = new LoginData(s.getUserName(), s.getPassword());
            ServerRequest<LogItem> serverRequest = new ServerRequest<LogItem>();
            serverRequest.data = item;
            serverRequest.login = loginData;
            
            jsonString = mapper.writeValueAsString(serverRequest);
            
           // RequestBase request = new RequestBase();
            String address = getAddress("/logs", null);
            String response = new ServicesClient().executePut( address, jsonString);

            result = true;


        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }
    }
    
    public void endLog(LogItem item) throws UnableToConnectException
    {
        boolean result = false;
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = "";
            
            SettingsDAO s = DatabaseManager.getInstance().getActiveSettings();
            LoginData loginData = new LoginData(s.getUserName(), s.getPassword());
            ServerRequest<LogItem> serverRequest = new ServerRequest<LogItem>();
            serverRequest.data = item;
            serverRequest.login = loginData;
            
            jsonString = mapper.writeValueAsString(serverRequest);
            
           // RequestBase request = new RequestBase();
            String address = getAddress("/logs", null);
            String response = new ServicesClient().executePost( address, jsonString);

            result = true;


        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }
    }
    
    public void saveConfiguration(DeviceDataList deviceDataList)
            throws UnableToConnectException
    {
        //Toast.makeText(StokerWebAndroid.getContext(), "Save not implemented", Toast.LENGTH_SHORT).show();
        boolean result = false;
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = "";
            
            SettingsDAO s = DatabaseManager.getInstance().getActiveSettings();
            LoginData loginData = new LoginData(s.getUserName(), s.getPassword());
            ServerRequest<DeviceDataList> serverRequest = new ServerRequest<DeviceDataList>();
            serverRequest.data = deviceDataList;
            serverRequest.login = loginData;
            
            jsonString = mapper.writeValueAsString(serverRequest);
            
           // RequestBase request = new RequestBase();
            String address = getAddress("/devices", null);
            String response = new ServicesClient().executePost( address, jsonString);

            TypeReference<CookerList> typeRef = new TypeReference<CookerList>() {
            //TypeReference<ServiceResult<DeviceDataList>> typeRef = new TypeReference<ServiceResult<DeviceDataList>>() {
            };
            
           // deviceData = JacksonObjectMapper.INSTANCE.mapper.readValue(response, typeRef);
            result = true;
            /*if (deviceData != null)
            {
                result = true;
           //     deviceData = serviceResult.get1stDataEntry();
            }
            else
            {
                Log.d("CookerList", "Call to server failed");
            }*/

        }
        catch (MalformedURLException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
            throw new UnableToConnectException();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
           throw new UnableToConnectException();
        }


    }

    public String testConnection(String address)
    {
        String message = "Success";
        try
        {
            String response = StokerWebAndroid.getServicesClient().executeGet(address);
            Log.d(TAG,"Test Response: " + response );
        }
        catch (ClientProtocolException e)
        {
            message = "ClientProtocolException";
            e.printStackTrace();
        }
        catch (IOException e)
        {
            message = e.getMessage();
            e.printStackTrace();
        }
        catch (Exception e)
        {
            message = e.getMessage();
        }

        return message;
    }



}
