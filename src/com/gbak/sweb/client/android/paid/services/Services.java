package com.gbak.sweb.client.android.paid.services;

import java.util.List;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.constants.ServerConnectionType;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.utils.PropertiesManager;
import com.gbak.sweb.client.android.paid.utils.SWUtil;

import android.util.Log;

 public class Services
{
     
     public StokerWebDataService stokerWebDataService;

    public Services() throws NoActiveSettingException 
    {
        super();
        
        DatabaseManager.init(StokerWebAndroid.getContext());
        
        Log.d("Services", "Services constructor called");
        PropertiesManager properties = StokerWebAndroid.getPropertiesManager();
        String servicesImpl = properties.getProperty("serviceType");
        
        if (servicesImpl.equalsIgnoreCase("MOCK"))
        {
            stokerWebDataService = new com.gbak.sweb.client.android.paid.services.mock.StokerWebDataService();
        
        }
        else if (servicesImpl.equalsIgnoreCase("SERVER"))
        {
           // final List<SettingsDAO> settingsList = DatabaseManager.getInstance().getSettings();
            SettingsDAO settings = DatabaseManager.getInstance().getActiveSettings();
            if ( settings != null )
            {
               // SettingsDAO settings = settingsList.get(0);
                
              
                String server = "STOKER";
                if ( settings != null && settings.getServerConnectionType() != null && settings.getServerConnectionType().length() > 0 )
                { 
                    server = settings.getServerConnectionType(); 
                }
                
                if (   server.compareTo(ServerConnectionType.STOKER.toString()) == 0 )
                    stokerWebDataService = new com.gbak.sweb.client.android.paid.services.stoker.StokerWebDataService();
                else
                    stokerWebDataService = new com.gbak.sweb.client.android.paid.services.server.StokerWebDataService();
            }
        }

    }
}
