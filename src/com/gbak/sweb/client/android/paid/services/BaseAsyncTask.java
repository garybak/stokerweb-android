package com.gbak.sweb.client.android.paid.services;

import java.io.IOException;
import java.net.SocketTimeoutException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

public abstract class BaseAsyncTask<Params, Progress> extends AsyncTask<Params, Progress, Boolean> {
    protected String exceptionMessage = null;
    
    private String loadingMessage = null;
    private ProgressDialog dialog = null;
    private Activity activity = null;
    
    public void execute(Activity parentActivity, String loadingMessage, Params... params)
    {
        activity = parentActivity;
        if ( loadingMessage != null && loadingMessage.length() > 0)
        {
           this.loadingMessage = loadingMessage;
           dialog = new ProgressDialog(activity);
        }
        super.execute(params);
    }

    protected void onPreExecute() 
    {
        if ( loadingMessage != null && loadingMessage.length() > 0)
        {
            this.dialog.setMessage(loadingMessage);
            this.dialog.setCancelable(true);
            this.dialog.show();
        }
    }

    @Override
    protected Boolean doInBackground(Params... params) {
        try {
            return doWorkInBackground(params);
        }
        catch (SocketTimeoutException e) {
            exceptionMessage = "Failed to connect to the server. " + e.getMessage();
        }
        catch (IOException e)
        {
            exceptionMessage = "IOException occurred. " + e.getMessage();
        }
        catch (Throwable t)
        {
            exceptionMessage = "Error occurred. " + t.getMessage();
        }

        return false;
    }

    protected void onPostExecute(Boolean result) 
    {
        try 
        {
            if (exceptionMessage == null && result == false) 
            {
                exceptionMessage = "Service call failed. No exception thrown.";
            }
            
            if (exceptionMessage != null && activity != null) 
            {
              //  Utils.alert(exceptionMessage, activity, false);
            }
            
            onWorkPostExecute(result);
        }
        finally 
        {
            if ( dialog != null )
                if (this.dialog.isShowing()) 
                {
                    this.dialog.dismiss();
                }
        }
    }
    
    /**
     * Returns true if work is successful. Returns false if failed.
     * 
     * @param params
     * @return
     * @throws Throwable 
     */
    protected abstract Boolean doWorkInBackground(Params... params) throws Throwable;
    
    /**
     * The true/false value returned from doWorkInBackground will be pass to this method as result.
     * 
     * @param result
     */
    protected abstract void onWorkPostExecute(Boolean result);
}

