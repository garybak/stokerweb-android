package com.gbak.sweb.client.android.paid.services;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;


public class ServiceResult {

    ObjectMapper m_jacksonObjectMapper = new ObjectMapper();
    
    @JsonProperty(value="success")
    public boolean success;
    
    @JsonProperty(value="feedback")
    public String feedback;
    
    @JsonProperty(value="data")
    public String data;
    
    public ServiceResult(){}

    public ServiceResult(String json) throws Exception
    {
        ServiceResult temp = m_jacksonObjectMapper.readValue(json, ServiceResult.class);
        
        this.success = temp.success;
        this.feedback = temp.feedback;
        this.data = temp.data;
    }
    
    public String toJsonString() throws Exception
    {    
        return m_jacksonObjectMapper.writeValueAsString(this);
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }
    
    public void setSuccess(boolean success) {
        this.success = success;
    }

}
