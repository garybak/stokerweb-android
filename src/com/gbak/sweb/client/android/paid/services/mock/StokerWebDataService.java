package com.gbak.sweb.client.android.paid.services.mock;

import java.io.IOException;
import java.util.Calendar;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.base.CamelCaseNamingStrategy;
import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.json.CookerList;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;
import com.gbak.sweb.common.json.LogNote;
import com.gbak.sweb.common.json.Probe;


public class StokerWebDataService implements com.gbak.sweb.client.android.paid.services.StokerWebDataService
{

    public CookerList getCookerList()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public DeviceDataList getTempData()
    {
       //ArrayList<SDevice> testData = new ArrayList<SDevice>();
        DeviceDataList testData = new DeviceDataList();
        testData.receivedDate = Calendar.getInstance().getTime();
        Probe sp = new Probe("1233", "Temp Probe 1", "200", "", "", AlarmType.NONE , "200","cooker1" );
        testData.devices.add( sp );
        sp = new Probe("1234", "Temp Probe 2", "200", "", "", AlarmType.NONE , "2", "cooker1" );
        testData.devices.add( sp );
        sp = new Probe("1235", "Temp Probe 3", "200", "", "", AlarmType.ALARM_FIRE , "225", "cooker1" );
        
        sp = new Probe("1243", "Temp Probe 21", "200", "", "", AlarmType.NONE , "200","cooker2" );
        testData.devices.add( sp );
        sp = new Probe("1244", "Temp Probe 22", "200", "", "", AlarmType.NONE , "2", "cooker2" );
        testData.devices.add( sp );
        sp = new Probe("1245", "Temp Probe 23", "200", "", "", AlarmType.NONE , "225", "cooker2" );
        testData.devices.add( sp );
        
        
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy( new CamelCaseNamingStrategy());
        try
        {
            String s = objectMapper.writeValueAsString(SWUtil.addCookerBlanks(testData));
            System.out.println("testData: " + s);
        }
        catch (JsonGenerationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (JsonMappingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SWUtil.addCookerBlanks(testData);
    }

    public void saveConfiguration(DeviceDataList device)
            throws UnableToConnectException
    {
        // TODO Auto-generated method stub
    }

    public LogItemList getLogData(String cookerName)
            throws UnableToConnectException
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void newNote(LogNote ln) throws UnableToConnectException
    {
        // TODO Auto-generated method stub
        
    }

    public void newLog(LogItem item) throws UnableToConnectException
    {
        // TODO Auto-generated method stub
        
    }

    public void endLog(LogItem item) throws UnableToConnectException
    {
        // TODO Auto-generated method stub
        
    }

    public String getTestAddress(SettingsDAO s)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public String testConnection(String address)
    {
        // TODO Auto-generated method stub
        return "";
    }
    
}
