package com.gbak.sweb.client.android.paid.services.stoker;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.type.TypeReference;

import android.util.Log;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.constants.ServerConnectionType;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.ServicesClient;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.client.android.paid.utils.StokerPostData;
import com.gbak.sweb.common.base.JacksonObjectMapper;
import com.gbak.sweb.common.base.RequestBase;
import com.gbak.sweb.common.base.ServiceResult;
import com.gbak.sweb.common.json.CookerList;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;
import com.gbak.sweb.common.json.LogNote;
import com.gbak.sweb.common.json.StokerJson;

public class StokerWebDataService implements com.gbak.sweb.client.android.paid.services.StokerWebDataService
{

    StokerJson stokerData;
    DeviceDataList deviceData;
    final String TAG = "StokerWebDataService";
    
    public String  getAddress(String serviceContext, SettingsDAO s) throws NoActiveSettingException
    {
        String address;
        synchronized( this )
        {
            SettingsDAO settings = null;
            if ( s == null)
               settings = DatabaseManager.getInstance().getActiveSettings();
            else
                settings = s;
            if ( settings == null )
            {
                return "";
            }
            
            address = "http://" + settings.getHostname() + ":" + settings.getPort();

            address = address + serviceContext;
            
            Log.i(TAG,"Built stoker address: " + address);
        }
        return address;
    }
    
    public String getTestAddress(SettingsDAO s)
    {
        String address = null;
        try
        {
            address = getAddress("/stoker.json", s);
        }
        catch (NoActiveSettingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return address;
        
    }
    
    public void saveConfiguration( DeviceDataList d) throws UnableToConnectException, NoActiveSettingException
    {
        String postString = StokerPostData.getPostString(d.devices);
        Log.i(TAG,"Posting string: " + postString );
        String address = getAddress("/stoker.Post_Handler", null);
        try
        {
           // String response = StokerWebAndroid.getServicesClient().execute(address, postString);
            new ServicesClient().executePost(address,  postString);

        }
        catch (ClientProtocolException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();

            throw new UnableToConnectException();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block

            e.printStackTrace();
            throw new UnableToConnectException();
        }
        

    }
    
    public DeviceDataList getTempData()
    {
        boolean result = false;
        try
        {
           // RequestBase request = new RequestBase();
            String address = getAddress("/stoker.json", null);
        //    Log.d("StokerWebDataService","Executing HTTP Get with address: " + address );
            Log.d(TAG, "Request sent: " + address );
            String response = StokerWebAndroid.getServicesClient().executeGet(address);

            TypeReference<StokerJson> typeRef = new TypeReference<StokerJson>() { };
            
            stokerData = JacksonObjectMapper.INSTANCE.mapper.readValue(response, typeRef);
            Log.d(TAG, "Response from stoker ");
            if (stokerData != null)
            {
                result = true;
                deviceData = SWUtil.convertToDeviceData( stokerData );
            }
            else
            {

                Log.i(TAG, "Call to server failed");
            }

        }
        catch (MalformedURLException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch (SocketTimeoutException e1)
        {
            e1.printStackTrace();

        }
        catch (IOException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        catch (NumberFormatException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }

        return deviceData;
    }

    public CookerList getCookerList()
    {
        
        boolean result = false;
        
        Log.e(TAG, "getCookerList() not implemented for stoker connection");
        
        return null;
        
    }

    public LogItemList getLogData(String cookerName)
            throws UnableToConnectException
    {
        boolean result = false;
        
        Log.e(TAG, "getLogData() not implemented for stoker connection");
        
        return null;
    }

    public void newNote(LogNote ln) throws UnableToConnectException
    {
        Log.e(TAG, "newNote() not implemented for stoker connection");  
    }

    public void newLog(LogItem item) throws UnableToConnectException
    {
        Log.e(TAG, "newLog() not implemented for stoker connection");

    }

    public void endLog(LogItem item) throws UnableToConnectException
    {
        Log.e(TAG, "endLog() not implemented for stoker connection");
        
    }

    public String testConnection(String address)
    {
        String message = "Success";
        try
        {
            String response = StokerWebAndroid.getServicesClient().executeGet(address);
            Log.d(TAG,"Test Response: " + response );
        }
        catch (ClientProtocolException e)
        {
            message = "ClientProtocolException";
            e.printStackTrace();
        }
        catch (IOException e)
        {
            message = e.getMessage();
            e.printStackTrace();
        }
        catch (Exception e)
        {
            message = e.getMessage();
        }

        return message;
    }


}
