package com.gbak.sweb.client.android.paid.services;


import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.common.json.CookerList;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;
import com.gbak.sweb.common.json.LogNote;


public interface StokerWebDataService
{

    CookerList getCookerList() throws UnableToConnectException;
    DeviceDataList getTempData() throws UnableToConnectException;
    void saveConfiguration( DeviceDataList device) throws UnableToConnectException, NoActiveSettingException;
    String getTestAddress( SettingsDAO s );
    LogItemList getLogData( String cookerName ) throws UnableToConnectException;
    void newNote(LogNote ln) throws UnableToConnectException;
    void newLog(LogItem item) throws UnableToConnectException;
    void endLog(LogItem item) throws UnableToConnectException;
    String testConnection( String address );
    
    
}
