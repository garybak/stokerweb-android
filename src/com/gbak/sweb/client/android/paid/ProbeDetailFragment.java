package com.gbak.sweb.client.android.paid;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlertStash;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.base.constant.AlarmType;

import com.gbak.sweb.client.android.paid.R;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ProbeDetailFragment extends Fragment 
{
    
    private final String TAG = "ProbeDetailFragment";
    
    TextView probeName;
    TextView currentTemp;
    TextView targetTemp;
    Spinner alarmType;
    TextView alarmLow;
    TextView alarmHigh;
    ToggleButton alarmEnabled;
    TextView alarmIgnoreMinutes;
    Spinner blowerName;
    String  probeID;
    CheckBox alarmIgnore;
    ProbeAlert alert;
    
    DeviceStashDAO stashedDevice;
    boolean alertUpdated = false;
    int alertIgnoreMinutes = 0;
    
    Context context = null;

//    @Override
//    public void onCreate(Bundle savedInstanceState) 
//    {
  
    public static ProbeDetailFragment newInstance(String title)
    {

        ProbeDetailFragment probeDetailFragment = new ProbeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        
        probeDetailFragment.setArguments(bundle);
        return probeDetailFragment;
    }
    
      @Override
      public View onCreateView(LayoutInflater inflater, 
                               ViewGroup container,
                               Bundle savedInstanceState) 
      {

        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.page_probe_detail, container, false);
        
        context = view.getContext();
        DatabaseManager.init( context );
        
       

        probeName = (TextView) view.findViewById(R.id.probeDetail_probeName);
        currentTemp = (TextView) view.findViewById(R.id.probeDetail_CurrentTemp);
        targetTemp = (TextView) view.findViewById(R.id.probeDetail_targetTemp);
        alarmType = (Spinner) view.findViewById(R.id.probeDetail_spinner_AlarmType);
        alarmLow = (TextView) view.findViewById(R.id.probeDetail_text_AlarmLow);
        alarmHigh = (TextView) view.findViewById(R.id.probeDetail_text_AlarmHigh);
        alarmEnabled = (ToggleButton) view.findViewById(R.id.probeDetail_toggle_AlarmEnabled);
        alarmIgnore = (CheckBox) view.findViewById(R.id.probeDetail_checkbox_ignoreAlarmEnabled);
        alarmIgnoreMinutes = (TextView) view.findViewById(R.id.probeDetail_text_IgnoreMinutes);
        blowerName = (Spinner) view.findViewById(R.id.probeDetail_spinner_BlowerSelection);

        probeID = getArguments().getString("id");
        stashedDevice = DatabaseManager.getInstance().getDeviceStash(probeID);
         
         probeName.setText(stashedDevice.getName());
         currentTemp.setText( String.valueOf(stashedDevice.getCurrentTemp()));
         targetTemp.setText( String.valueOf(stashedDevice.getTargetTemp()));
         
         alarmLow.setText( String.valueOf(stashedDevice.getLowerTempAlarm()));
         alarmHigh.setText( String.valueOf(stashedDevice.getUpperTempAlarm()));
         
         probeName.setOnClickListener(editProbeNameHandler);
         
         List<String> list = new ArrayList<String>();
         for ( AlarmType at : AlarmType.values())
         {
             list.add(at.toString());
             Log.d(TAG,"Enum String: " + at.toString() + " Name: " + at.name());
         }
         //list.add(AlarmType.NONE.toString());
         //list.add(AlarmType.ALARM_FIRE.toString());
         //list.add(AlarmType.ALARM_FOOD.toString());
         
         ArrayAdapter<String> alarmTypeAdapter = new ArrayAdapter<String>( view.getContext(),android.R.layout.simple_spinner_item, list );
         alarmTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         alarmType.setAdapter( alarmTypeAdapter );
         
         int alarmTypeInt = stashedDevice.getAlarmType().ordinal();
         alarmType.setSelection( alarmTypeInt );
        
         alert = DatabaseManager.getInstance().getProbeAlarm(probeID);
         if ( alert == null )
         {
             alert = SWUtil.getInstance().addDefaultAlarm(probeID);
         }
      
         
         alarmEnabled.setChecked(alert.isEnabled());
         String alarmIgnoreMinTemp = new String("0");
         if ( alert.isEnabled() && alert.isIgnoreTimerEnabled() && alert.getIgnoreTimerReEnableTime() != null )
         {
             Date now  = Calendar.getInstance().getTime();
             Date enableAlarm = alert.getIgnoreTimerReEnableTime();
             if ( now.before(enableAlarm))
             {
                 alertIgnoreMinutes = (int)((alert.getIgnoreTimerReEnableTime().getTime()/60000) -
                                    ( now.getTime() / 60000 ));
                 Log.d("ProbeDetail","Calcualted time difference is: " + alertIgnoreMinutes );
                 alarmIgnoreMinTemp = String.valueOf(alertIgnoreMinutes);
             }
             else
             {
                 alert.setIgnoreTimerEnabled(false);
                 alert.setIgnoreTimerReEnableTime(null);
                 alarmIgnoreMinTemp = "0";
             }
         }
         alarmIgnore.setChecked( alert.isIgnoreTimerEnabled());
         alarmIgnoreMinutes.setText(alarmIgnoreMinTemp);
         
         // Enable / Disable fields
         
         enableDisableAlarmFields( alarmTypeInt );
         enableDisableAlartFields();
         
      //   getAvailableBlowers();
         List<String> blowerList = new ArrayList<String>();
        /* for ( AlarmType at : AlarmType.values())
         {
             blowerList.add(at.toString());
         }*/
         
         String bn =stashedDevice.getBlowerName();
         if ( bn == null || bn.length() == 0)
             bn = "None";
         else
            blowerList.add(bn);
         
         ArrayAdapter<String> blowerAdapter = new ArrayAdapter<String>( view.getContext(),android.R.layout.simple_spinner_item, blowerList );
         blowerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         blowerName.setAdapter( blowerAdapter );
         
         // Blower field
         if (stashedDevice.getBlowerID().length() == 0 )
         {
             blowerName.setEnabled(false);
         }
         else
         {
             blowerName.setEnabled(false);  // TODO: make this true when implementing save
         }
        
         
         alarmType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
         {
             public void onItemSelected(AdapterView<?> adapterView, View view, int selectedPos, long l) 
             { 
                 enableDisableAlarmFields( selectedPos );
             } 

             public void onNothingSelected(AdapterView<?> adapterView) 
             {
                 enableDisableAlarmFields( AlarmType.NONE.ordinal() );
                 return;
             } 
         }); 
         
         alarmEnabled.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0)
            {
                enableDisableAlartFields();
                
            } 
         });
         
             
        return view;
    }

    
      private OnClickListener editProbeNameHandler = new OnClickListener()
      {
          public void onClick(View v)
          {
              
              final EditText input = new EditText(context);
              input.setSingleLine();
              input.setHint("name");
             
              int i = 0;
       
              Log.d(TAG,"Click");
              
                  new AlertDialog.Builder(context)
                  .setTitle("Probe Name")
                  .setView(input)
                  .setPositiveButton("Save", new DialogInterface.OnClickListener() 
                  {
                      public void onClick(DialogInterface dialog, int whichButton) 
                      {
                          probeName.setText(input.getText().toString());
                      }
                  }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
                  {
                      public void onClick(DialogInterface dialog, int whichButton) 
                      {
                          // Do nothing.
                      }
                  }).create().show();
          }
      };
      
    private void enableDisableAlartFields( )
    {
        boolean b = false;
        if ( alarmEnabled.isChecked() )
           b = true;
        
        alarmIgnore.setEnabled(b);
        alarmIgnoreMinutes.setEnabled( b);
    }
    
    private void enableDisableAlarmFields( int alarmTypeInt )
    {
        if ( alarmTypeInt == AlarmType.NONE.ordinal())
        {
            alarmHigh.setEnabled(false);
            alarmLow.setEnabled(false);
        }
        else if ( alarmTypeInt == AlarmType.ALARM_FIRE.ordinal())
        {
            alarmHigh.setEnabled(true);
            alarmLow.setEnabled(true);
        }
        else if ( alarmTypeInt == AlarmType.ALARM_FOOD.ordinal())
        {
            alarmHigh.setEnabled(false);
            alarmLow.setEnabled(false);
        }
    }
    
    private void setupHandlers()
    {
        
    }
    
    private int getIntWithDefault( String s )
    {
        int i = 0;
        try
        {
            i = Integer.valueOf( s );
        }
        catch ( NumberFormatException nfe )
        {
            i = 0;
        }
        return i;
    }
    private void populateDevice()
    {

      String pn = probeName.getText().toString();
      if ( stashedDevice.getName().compareTo(pn) != 0)
      {
          stashedDevice.setUpated(true);
          stashedDevice.setName(pn);
          Log.d("ProbeDetail","updated probeName");
      }
      int tt = getIntWithDefault(targetTemp.getText().toString());
      if ( stashedDevice.getTargetTemp() != tt )
      {
          stashedDevice.setUpated(true);
          stashedDevice.setTargetTemp( tt );
          Log.d("ProbeDetail","updated targetTemp");
      }
      int at = alarmType.getSelectedItemPosition();
      if ( stashedDevice.getAlarmType().ordinal() != at )
      {
          stashedDevice.setUpated(true);
          stashedDevice.setAlarmType( AlarmType.values()[at]);
          Log.d("ProbeDetail","updated alarmType");
      }
      int al = getIntWithDefault(alarmLow.getText().toString());
      if ( stashedDevice.getLowerTempAlarm() != al )
      {
          stashedDevice.setUpated(true);
          stashedDevice.setLowerTempAlarm(al);
          Log.d("ProbeDetail","updated alarmLow");
      }
      int ah = getIntWithDefault(alarmHigh.getText().toString());
      if ( stashedDevice.getUpperTempAlarm() != ah )
      {
          stashedDevice.setUpated(true);
          stashedDevice.setUpperTempAlarm(ah);
          Log.d("ProbeDetail","updated alarmHigh");
      }
    //  int bn = blowerName.getSelectedItemPosition();
         
      DatabaseManager.getInstance().updateDeviceStash(stashedDevice);
   }
    
    
    private void populateAlert()
    {
        ProbeAlertStash alertStash = new ProbeAlertStash(alert);
        
        alertUpdated = false;
        if ( alarmEnabled.isChecked() != alert.isEnabled() )
        {
            alertStash.setUpated(true);
            alertStash.setEnabled( alarmEnabled.isChecked());
        }
        
        boolean ai = alarmIgnore.isChecked();
        if ( ai != alert.isIgnoreTimerEnabled() )
        {
            alertStash.setUpated(true);
            alertStash.setIgnoreTimerEnabled( ai );
        }
        
        if ( ai == true )
        {
            int ignoreMinutes = getIntWithDefault(alarmIgnoreMinutes.getText().toString());
            if ( ignoreMinutes != alertIgnoreMinutes )
            {
                alertStash.setUpated(true);
                alertStash.setMinutes(ignoreMinutes);
            }
        }
        
        DatabaseManager.getInstance().updateProbeAlertStash(alertStash);
    }
    
    public void saveSettings()
    {
        
        if ( probeName != null )
        {
            Log.d(TAG,"Saving settings for: " + probeName.getText().toString());
           populateDevice();
           populateAlert();
        }
        else
        {
            Log.d(TAG,"Detected null fragment during save, skipping");
        }
    }
    
    @Override
    public void onResume() 
    {
        super.onResume();
    }
    
    @Override
    public void onPause()
    {
      //  saveSettings();
        super.onPause();
    }
    
    @Override 
    public void onStop()
    {
        // Using onStop to save the settings was too slow.  Needed to disable the list view
        // so the use has to wait for the save to finish.
        super.onStop();
      //  saveSettings();
    }
  
    
}
