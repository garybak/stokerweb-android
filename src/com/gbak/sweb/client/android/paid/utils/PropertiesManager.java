package com.gbak.sweb.client.android.paid.utils;

import java.util.Properties;

public class PropertiesManager {
    private Properties properties = new Properties();

    public void setProperties(Properties p) {
        this.properties = p;
    }

    public Properties getProperties() {
        return properties;
    }
    
    public String getProperty(String key)
    {
        return properties.getProperty(key);
    }
    
    public String getProperty(String key, String defaultVal)
    {
        return properties.getProperty(key, defaultVal);
    }
}

