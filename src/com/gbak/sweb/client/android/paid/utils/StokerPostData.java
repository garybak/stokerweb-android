package com.gbak.sweb.client.android.paid.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.json.Blower;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.PitProbe;
import com.gbak.sweb.common.json.Probe;

//import android.util.Log;

public class StokerPostData
{

    private static String alPostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof Probe )
       {
           int at = ((Probe)sd).alarmType.ordinal();
             return "al" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                           URLEncoder.encode(String.valueOf(at), "UTF-8");
       }
       return "";
    }

    private static String n1PostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof Probe )
       {
             return "n1" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                           URLEncoder.encode( sd.Name, "UTF-8");

       }
       return "";
    }

    private static String n2PostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof Blower )
             return "n2" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                           URLEncoder.encode( sd.Name, "UTF-8");
       return "";
    }

    private static String swPostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof PitProbe )
       {
           Blower sf = ((PitProbe) sd).blower;
           String fanID = null;
           if ( sf == null )
               fanID = "None";
           else
               fanID = sf.id.toUpperCase();

           return "sw" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                   URLEncoder.encode( fanID , "UTF-8");

       }
        return "";
    }

    private static String taPostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof Probe )
             return "ta" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                           URLEncoder.encode( String.valueOf(((Probe) sd).targetTemp).toString(), "UTF-8");
       return "";
    }

    private static String thPostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof Probe )
       {
           String alarmHigh = "n/a";

           if ( ((Probe) sd).alarmType == AlarmType.ALARM_FIRE )
           {
              alarmHigh = String.valueOf(((Probe) sd).upperTempAlarm).toString();
           }

           return "th" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                   URLEncoder.encode( alarmHigh , "UTF-8");

       }
        return "";
    }

    private static String tlPostData( Device sd ) throws UnsupportedEncodingException
    {
       if ( sd instanceof Probe )
       {
           String alarmLow = "n/a";

           if ( ((Probe) sd).alarmType == AlarmType.ALARM_FIRE )
           {
              alarmLow = String.valueOf(((Probe) sd).lowerTempAlarm).toString();
           }

           return "tl" + URLEncoder.encode(sd.id.toUpperCase(), "UTF-8") + "=" +
                   URLEncoder.encode( alarmLow , "UTF-8");
       }
        return "";
    }


    private static String getPostData( Device sd )
    {
       StringBuilder sb = new StringBuilder();
       ArrayList<String> alPostData = new ArrayList<String>();
       try
       {
           String s = null;
           s = alPostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

           s = n1PostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

           s = n2PostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

           s = swPostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

           s = taPostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

           s = thPostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

           s = tlPostData(sd);
           if ( s.length() > 0 )
               alPostData.add( s );

       }
       catch (UnsupportedEncodingException uee)
       {
          // Log.i("StokerPostData", uee.getStackTrace().toString() );
        //  System.out.println("Unsupported Character while encoding URL");
       }

       int size = alPostData.size();
       for ( int i = 0; i < size; i++ )
       {
           sb.append(  alPostData.get( i ));
           if ( i < size - 1 )
           {
               sb.append( "&");
           }
       }
       return sb.toString();
    }

    
    public static String getPostString( ArrayList<Device> stokerDeviceList)
    {
        StringBuilder postData = new StringBuilder();
        try
        {
            
            int size = stokerDeviceList.size();
            for ( int i = 0; i < stokerDeviceList.size(); i++ )
            {
                Device sd = stokerDeviceList.get( i );
                /*SDevice hwDevice = m_HWConfig.get( sd.getID() );
                if ( hwDevice instanceof StokerPitProbe )
                {
                    
                    ((StokerPitProbe)hwDevice).update((StokerPitProbe) sd );
                }
                else if ( hwDevice instanceof StokerProbe )
                {
                    ((StokerProbe)hwDevice).update((StokerProbe) sd );
                }
                else if ( hwDevice instanceof StokerFan )
                {
                    ((StokerFan)hwDevice).update((StokerFan)sd );
                }*/
                
                postData.append( getPostData( sd ));
                if ( i < size - 1)
                    postData.append("&");
            }

         //   Log.d("StokerPostData","Post String: " + postData.toString());
/*
            // Send data
            String strStokerIP = StokerWebProperties.getInstance().getProperty(StokerWebConstants.PROPS_STOKER_IP_ADDRESS);
            URL url = new URL("http://" + strStokerIP + "/stoker.Post_Handler");
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData.toString());
            writer.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null)
            {
                logger.debug("Response: " + line );
            }
            writer.close();
            rd.close();
            */
            
          
        } 
        catch (Exception e) 
        {
            
        }
        
        return postData.toString();
    }

}
