package com.gbak.sweb.client.android.paid.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.app.NotificationManager;
import android.util.Log;

import com.gbak.sweb.client.android.paid.StokerWebAndroid;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.constants.ServerConnectionType;
import com.gbak.sweb.client.android.paid.data.dao.DeviceDAO;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.json.Alert;
import com.gbak.sweb.common.json.AlertType;
import com.gbak.sweb.common.json.Blower;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.PitProbe;
import com.gbak.sweb.common.json.Probe;
import com.gbak.sweb.common.json.StokerJson;

import com.gbak.sweb.common.base.constant.DeviceType;

public class SWUtil
{
    static SWUtil swutil;
   public String authtoken;
 //   private SettingsDAO settings;
    
    private SWUtil() 
    { 
        DatabaseManager.init( StokerWebAndroid.getContext());
    }
    
    public static SWUtil getInstance()
    {
        if ( swutil == null )
           swutil = new SWUtil();
        return swutil;
    }
    
    static 
    {
    //      authtoken =  properties.getProperty("authentication");
    }
    
    public static DeviceDataList addCookerBlanks( DeviceDataList ddl )
    {
        DeviceDataList outDDL = new DeviceDataList();
        if ( ddl != null )
        {
            outDDL.receivedDate = ddl.receivedDate;
            outDDL.logCount = ddl.logCount;
            String last = "";
            for ( Device d : ddl.devices )
            {
                if ( d.cooker == null || d.cooker.length() == 0 )
                    continue;
                
                if ( last.compareTo(d.cooker) != 0 )
                {
                    Device newD = new Device(null,"",d.cooker);
                    outDDL.devices.add( newD );
                    last = d.cooker;
                }
                outDDL.devices.add( d );
            }
            if ( outDDL.devices.size() == 0 )
                outDDL = ddl;
        }
        
        return outDDL;
    }
    
/*    public SettingsDAO getSettings()
    {
        if ( settings == null )
            initSettings();
        return settings;
    }
    
    private boolean initSettings()
    {
        settings = null;
        SettingsDataSource settingsDataSource = new SettingsDataSource( StokerWebAndroid.getContext() );
        settingsDataSource.open();
        
        List<SettingsDAO> values = settingsDataSource.getSettings();
        if ( values.size() > 0 )
        {
           settings = values.get(0);
        }
        settingsDataSource.close();

        return settings != null;
    }*/
    /*
    public String  getAddress(String serviceContext)
    {
        String address;
        synchronized( this )
        {
            SettingsDAO settings = DatabaseManager.getInstance().getSettings().get(0);
            if ( settings == null )
            {
                return "";
            }
            
            address = "http://" + settings.getHostname() + ":" + settings.getPort();
            
            if ( settings.getServerConnectionType().compareTo(ServerConnectionType.STOKER.toString() ) == 0 )
            {
                address = address + serviceContext;
            }
            else
            {
                String baseURL = "";
                if ( settings.getBaseURL() != null)
                    if ( settings.getBaseURL().length() > 0)
                        baseURL = "/" + settings.getBaseURL();
                
                
                address =  address + baseURL + "/stokerweb/api/v1" + serviceContext;
            }
        }
        return address;
    }*/

    public static AlarmType getAlarmType( String alarm )
    {
        return AlarmType.values()[Integer.valueOf(alarm)];
    }
    
    /**
     * Convert data from Stoker json format to the DeviceData format
     * @param stokerData json format directly from Stoker
     * @return
     */
    public static DeviceDataList convertToDeviceData( StokerJson stokerData )
    {
       DeviceDataList deviceData = new DeviceDataList();
      
       HashMap<String,Blower>  blowerHash = new HashMap<String,Blower>();
       
       if ( stokerData.stoker.blowers != null )
       {
	       for ( StokerJson.Blowers blower : stokerData.stoker.blowers )
	       {
	           //boolean on = blower.on.compareToIgnoreCase("on") == 0 ? false : true;
	           boolean on = blower.on.compareToIgnoreCase("0") == 0 ? false : true;
	           Blower b = new Blower( blower.id, blower.name, on, Long.valueOf(0) );
	           blowerHash.put( blower.id, b );
	       } 
       }
       deviceData.receivedDate = Calendar.getInstance().getTime();
       for ( StokerJson.Sensors sensor : stokerData.stoker.sensors )
       {
           if ( sensor.blower != null && sensor.blower.length() > 0 )
           {
               AlarmType at = getAlarmType( sensor.al );
               
               PitProbe pit = new PitProbe( sensor.id,
                                            sensor.name,
                                            sensor.ta,
                                            sensor.tl,
                                            sensor.th,
                                            at,
                                            sensor.tc,
                                            blowerHash.get(sensor.blower ),
                                            "");
           //    blowerHash.remove(sensor.blower);  // Put all blowers in available list
               deviceData.devices.add( pit );
           }
           else
           {
               AlarmType at = getAlarmType( sensor.al );
               
               Probe probe     = new Probe( sensor.id,
                                            sensor.name,
                                            sensor.ta,
                                            sensor.tl,
                                            sensor.th,
                                            at,
                                            sensor.tc,
                                            "");
               deviceData.devices.add(probe);
           }
       }
       
       // TODO: blower enhancement
     /*  for ( Blower b : blowerHash.values())
       {
           deviceData.availableBlowers.add(b);
       }*/
       return deviceData;
    }
    
    public static Alert checkAlarmsOnDevice(HashMap<String,ProbeAlert> alertMap, Device d )
    {
        String message = "";
        Date date = Calendar.getInstance().getTime();
        Date currentDate = Calendar.getInstance().getTime();
        
            if ( d.id == null)
                return null;
         
            ProbeAlert pa = alertMap.get(d.id);
            if ( pa == null )
            {
                pa  = new ProbeAlert(d.id, true, false, null );
                DatabaseManager.getInstance().addProbeAlarm(pa);
            }
            else
            {
               try
               {
                   if ( pa.isEnabled() == false ||
                        pa.isIgnoreTimerEnabled() &&  currentDate.before(pa.getIgnoreTimerReEnableTime()) )
                    return null;
               }
               catch ( NullPointerException e)
               {
                  return null;
               }
            }
            
            Alert alert = new Alert();
            
            alert.alertTime = date;
            alert.type = AlertType.NONE;
            
            if ( d.id != null && d instanceof Probe )
            {
                Probe p = (Probe) d;
                switch ( p.alarmType )
                {
                    case NONE:
                       break;
                    case ALARM_FOOD:
                        if ( Float.valueOf(p.currentTemp).intValue() >= Float.valueOf(p.targetTemp).intValue() )
                        {
                            alert.type = AlertType.TARGET_TEMP;
                            message = "Target temp ("+p.targetTemp+") reached.  Temp: " + p.currentTemp;
                        }
                            
                        break;
                    case ALARM_FIRE:
                        if ( Float.valueOf(p.currentTemp).intValue() > Float.valueOf(p.upperTempAlarm).intValue() )
                        {
                            alert.type = AlertType.HIGH_TEMP;
                            message = "Temp exceeded "+p.upperTempAlarm+".  Temp: " + p.currentTemp;
                        }
                        else if ( Float.valueOf(p.currentTemp).intValue() < Float.valueOf(p.lowerTempAlarm).intValue() )
                        {
                            alert.type = AlertType.LOW_TEMP;
                            message = "Temp dropped below "+p.lowerTempAlarm+".  Temp: " + p.currentTemp;
                        }
                     
                        break;
                }
                if ( alert.type != AlertType.NONE )
                {
                    alert.message = message;
                    alert.id = p.id;
                    alert.name = p.Name;
                            
                    
                    
                }
            } // end instanceof
            else
            {
                Log.e("SWUtil","DeviceDAO not instance of probe");
            }
       
        return alert;
    }
    
    public static HashMap<String,ProbeAlert> getProbeAlertMap()
    {
        HashMap<String,ProbeAlert> alertMap = new HashMap<String,ProbeAlert>();
        
        List<ProbeAlert> probeAlertList = DatabaseManager.getInstance().getProbeAlarms();
        for ( ProbeAlert pa : probeAlertList )
        {
            alertMap.put( pa.getProbeID(), pa);
        }
        
        return alertMap;
    }
    /**
     * Check the temperatures in the deviceList for alert conditions and return a list
     * of alert items that meet the alert criteria.
     * This method is used when there is no Alert REST call, like a direct call to the
     * stoker.
     * 
     * @param deviceDataList
     * @return
     */
    public static ArrayList<Alert> checkAlarmsAndAlert( DeviceDataList deviceDataList )
    {
        ArrayList<Alert> alertList = new ArrayList<Alert>();
        HashMap<String,ProbeAlert> alertMap = getProbeAlertMap();
        
        String message = "";
        Date date = Calendar.getInstance().getTime();
         
        if ( deviceDataList != null && deviceDataList.devices != null)
        {
       //     Date currentDate = Calendar.getInstance().getTime();
            Alert alert = null;
            for ( Device d : deviceDataList.devices )
            {
               alert = checkAlarmsOnDevice(alertMap, d );
               if ( alert != null && alert.id != null  )
                   alertList.add( alert );
            }
            
        }
        return alertList;
    }
    
    public ProbeAlert addDefaultAlarm( String probeID )
    {
        ProbeAlert pa = new ProbeAlert( probeID, true, false, null );
        
        DatabaseManager.getInstance().addProbeAlarm( pa );
        return DatabaseManager.getInstance().getProbeAlarm(probeID);
        
    }
  
}

