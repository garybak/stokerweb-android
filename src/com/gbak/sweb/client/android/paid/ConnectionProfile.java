package com.gbak.sweb.client.android.paid;

import java.util.ArrayList;
import java.util.List;

import com.gbak.sweb.client.android.paid.convert.DAOConversionUtil;
import com.gbak.sweb.client.android.paid.data.DatabaseManager;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.client.android.paid.data.dao.SettingsDAO;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.Probe;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class ConnectionProfile extends Activity
{

    private final String TAG = "ConnectionProfile";
    private ConnectionProfileItemAdapter adapter = null;
    private Context context = null;
    private List<SettingsDAO> settings = null;
            
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection_profile);
        context = this;
        setTitle("Profiles");
        load_settings();
        
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        
        //load_settings();
        settings = DatabaseManager.getInstance().getSettings();
        adapter.update(settings);
        adapter.notifyDataSetChanged();
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.connection_profile, menu);
        
        
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        if ( item.getItemId() == R.id.profile_new)
        {
            Log.d(getLocalClassName(),"New Profile Selected");
            loadSettingsActivity();
        }

        return false;
    }
    
    private void loadSettingsActivity()
    {
        Intent intent = new Intent( getBaseContext(), SettingsActivity.class);

        intent.putExtra("settingsID", "");
        startActivityForResult( intent,0 );
        
    }
    
        
    private void load_settings()
    {
        settings = DatabaseManager.getInstance().getSettings();
        
        adapter = new ConnectionProfileItemAdapter(this, android.R.layout.simple_list_item_1, settings );
        ListView listView = (ListView) findViewById(R.id.list_view_connection_profile);

        listView.setAdapter(adapter);
        
        listView.setOnItemLongClickListener(new OnItemLongClickListener() 
        {

            public boolean onItemLongClick(AdapterView<?> arg0, final View view,
                    final int position, long id)
            {
                //Dialog profileEdit = new ConnectionProfileEditDialog( context );
               // profileEdit.show();
                
                CharSequence [] ca = { "Edit Item","Delete Item" };
                new AlertDialog.Builder(context)
                .setTitle("Edit or Delete?")
                .setItems(ca,new OnClickListener() {

                    public void onClick(DialogInterface dialog, int which)
                    {
                        Log.d(TAG, "Item clicked");
                        if ( which == 0 )
                        {
                           Intent detailIntent = new Intent( view.getContext(),SettingsActivity.class);
                           long settingsID = settings.get(position).getId();
                           detailIntent.putExtra("settingsID", String.valueOf(settingsID));

                           startActivity(detailIntent);
                           
                        }
                        else if ( which == 1 )
                        {
                            new AlertDialog.Builder( context )
                            .setMessage("Delete Profile: " + settings.get(position).getProfileName() + "?")
                            .setPositiveButton("Yes", new OnClickListener() {

                                public void onClick(DialogInterface arg0,
                                        int arg1)
                                {
                                    long settingsID = settings.get(position).getId();
                                    DatabaseManager.getInstance().deleteSettings(String.valueOf(settingsID));
                                    adapter.update(DatabaseManager.getInstance().getSettings());
                                    adapter.notifyDataSetChanged();
                                }
                                
                            })
                            .setNegativeButton("Cancel", new OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) { }
                                
                            }).create().show();
                        }
                    }
                })
                .create().show();
                
                return true;
            }
            
        });

        listView.setOnItemClickListener( new OnItemClickListener() 
        {

            public void onItemClick(AdapterView<?> arg0, View view, int position, long id)
            {
                long settingsID = settings.get(position).getId();
               DatabaseManager.getInstance().setActiveSetting( String.valueOf(settingsID ));
               try
                {
                    StokerWebAndroid.refreshServices();
                }
                catch (NoActiveSettingException e)
                {
                    Log.e(TAG,"Setting active setting failed");
                    e.printStackTrace();
                }
               finish();
            }
        });
        
        listView.setEmptyView( (TextView) findViewById( R.id.empty_list));
    }
}
