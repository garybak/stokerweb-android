package com.gbak.sweb.client.android.paid;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.gbak.sweb.client.android.paid.services.Services;
import com.gbak.sweb.client.android.paid.services.ServicesClient;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.utils.PropertiesManager;

import android.content.Context;
import android.app.Application;
import android.content.res.Resources;

import com.gbak.sweb.client.android.paid.R;

public class StokerWebAndroid extends Application
{
    private static PropertiesManager propertiesManager = null;
    private static String deviceName = "";
    private static Context context;
    private static Services services;
    private static ServicesClient servicesClient;
    
    
    public void onCreate()
    {
        super.onCreate();
        StokerWebAndroid.context = getApplicationContext();
        
       // StokerWebAndroid.clientUser = new ClientUser();
       // getDeviceId();
        setDeviceName(android.os.Build.MODEL);
       // loadRegistrationFile();
        
        // Load Properties File
        Resources resources = this.getResources();
        
        InputStream inputStream;
        try 
        {
            inputStream = resources.openRawResource(R.raw.stokerwebandroid);
            Properties properties = new Properties();
            properties.load(inputStream);
            
            propertiesManager = new PropertiesManager();
            propertiesManager.setProperties(properties);
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }

        init();
    }
    
    private void init()
    {
       try
        {
            services = new Services();
        }
        catch (NoActiveSettingException e)
        {
    
            e.printStackTrace();
        }
        servicesClient = new ServicesClient();
    }
    
    public static void setDeviceName(String deviceName) {
        StokerWebAndroid.deviceName = deviceName;
    }
    
    public static PropertiesManager getPropertiesManager()
    {
        return propertiesManager;
    }
    
    public static void refreshServices() throws NoActiveSettingException
    {
        services = new Services();
    }
    public static Services getServices() throws NoActiveSettingException
    {
        if ( services == null )
            services =  new Services();
        
        return services;
    }
    
    public static Context getContext()
    {
        return context;
    }
    
    public static ServicesClient getServicesClient()
    {
        return servicesClient;
    }
    
}
