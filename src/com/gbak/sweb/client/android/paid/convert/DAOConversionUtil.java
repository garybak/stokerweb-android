package com.gbak.sweb.client.android.paid.convert;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.gbak.sweb.client.android.paid.data.dao.DeviceDAO;
import com.gbak.sweb.client.android.paid.data.dao.DeviceStashDAO;
import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.base.constant.DeviceType;
import com.gbak.sweb.common.json.Blower;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogItemList;
import com.gbak.sweb.common.json.PitProbe;
import com.gbak.sweb.common.json.Probe;

public class DAOConversionUtil
{
    
    public static DeviceStashDAO convertToDeviceStashDAO( Device d )
    {
        DeviceStashDAO du = new DeviceStashDAO(convertToDeviceDao(d));
        du.setUpated(false);

        return du;
    }
    
    public static LogItemList convertToLogItemList( List<DeviceDAO> dao )
    {
       LogItemList lil = new LogItemList();
       
       return lil;
    }
    
    public static List<Device> convertToList( List<DeviceDAO> dao)
    {
        List<Device> ddl = new ArrayList<Device>();
        String last = "";
        int count = 0;
        for ( DeviceDAO dd : dao )
        {
            Device d = convertDeviceDAOToDevice( dd );
            ddl.add( d );
            if ( d instanceof PitProbe)
            {
                ddl.add( (Device)((PitProbe)d).blower);
            }
        }
        
        return ddl;
    }
    
    public static DeviceDataList convertToDataList( List<DeviceDAO> dao)
    {
        DeviceDataList ddl = new DeviceDataList();
        String last = "";
        int count = 0;
        for ( DeviceDAO dd : dao )
        {
            if ( count == 0 )
                ddl.receivedDate = dd.getLastUpdateTime();
            
            Device d = convertDeviceDAOToDevice( dd );
            if ( d.cooker.length() != 0 && last.compareTo(d.cooker) != 0 )
            {
                Device newD = new Device(null,"",d.cooker);
                ddl.devices.add( newD );
                last = d.cooker;
            }    
            ddl.devices.add( d );
               
        }
        
        return ddl;
    }

    public static List<DeviceDAO> convertToDeviceDaoList( LogItemList  lil )
    {
        
        List<DeviceDAO> daoList = new ArrayList<DeviceDAO>();
        
        for ( LogItem li : lil.logList )
        {
            
            for ( Device d : li.deviceList )
            {
                DeviceDAO dao = convertToDeviceDao( d );
                dao.setLastUpdateTime(lil.receivedDate);
                daoList.add( dao ) ;   
            }
        }
        return daoList;
    }
    
    public static List<DeviceDAO> convertToDeviceDaoList( DeviceDataList ddl )
    {
        
        List<DeviceDAO> daoList = new ArrayList<DeviceDAO>();
        if ( ddl != null && ddl.devices != null )
            for ( Device d : ddl.devices )
            {
                DeviceDAO dao = convertToDeviceDao( d );
                dao.setLastUpdateTime(ddl.receivedDate);
                daoList.add( dao ) ;   
            }
        return daoList;
    }
    
    public static DeviceDAO convertToDeviceDao( Device d )
    {
        if ( d instanceof PitProbe )
            return convertToDeviceDao((PitProbe) d);
        else if ( d instanceof Probe )
            return convertToDeviceDao((Probe) d);
        else if ( d instanceof Blower )
            return convertToDeviceDao((Blower) d);
        
        Log.e("DAOConversionUtil","Unable to determine class type for conversion");
        return null;    
    }
    public static DeviceDAO convertToDeviceDao( PitProbe pitProbe )
    {
        DeviceDAO dao = new DeviceDAO();
        
        dao.setId(pitProbe.id);
        dao.setName(pitProbe.Name);
        dao.setCooker(pitProbe.cooker);
        dao.setAlarmType(pitProbe.alarmType);
        dao.setDeviceType(DeviceType.PIT_PROBE);
        dao.setCurrentTemp(floatValueOf(pitProbe.currentTemp));
        dao.setTargetTemp(intValueOf(pitProbe.targetTemp));
        dao.setLowerTempAlarm(intValueOf(pitProbe.lowerTempAlarm));
        dao.setUpperTempAlarm(intValueOf(pitProbe.upperTempAlarm));
        if ( pitProbe.blower != null )
        {
           dao.setBlowerID(pitProbe.blower.id);
           dao.setBlowerName(pitProbe.blower.Name);
           dao.setBlowerOn(pitProbe.blower.fanOn);
           dao.setBlowerRunTime(pitProbe.blower.totalRuntime);
        }
        else
        {
            dao.setBlowerID("");
            dao.setBlowerName("");
            dao.setBlowerOn(false);
            dao.setBlowerRunTime(0);
        }
       
        return dao;
    }
    
    public static DeviceDAO convertToDeviceDao( Probe probe )
    {
        DeviceDAO dao = new DeviceDAO();
        
        dao.setId(probe.id);
        dao.setName(probe.Name);
        dao.setCooker(probe.cooker);
        dao.setAlarmType(probe.alarmType);
        dao.setDeviceType(DeviceType.PROBE);
        dao.setCurrentTemp(floatValueOf(probe.currentTemp));
        dao.setTargetTemp(intValueOf(probe.targetTemp));
        dao.setLowerTempAlarm(intValueOf(probe.lowerTempAlarm));
        dao.setUpperTempAlarm(intValueOf(probe.upperTempAlarm));
        dao.setBlowerID("");
        dao.setBlowerName("");
        dao.setBlowerOn(false);
        dao.setBlowerRunTime(0);
       
        return dao;
    }
    
    public static DeviceDAO convertToDeviceDao( Blower blower )
    {
        DeviceDAO dao = new DeviceDAO();
        
        dao.setId("");  // blower.id
        dao.setName("");
        dao.setCooker("");
        dao.setAlarmType(AlarmType.NONE);
        dao.setDeviceType(DeviceType.BLOWER);
        dao.setTargetTemp(0);
        dao.setLowerTempAlarm(0);
        dao.setUpperTempAlarm(0);
        if ( blower != null )
        {
            dao.setBlowerID(blower.id);
            dao.setBlowerName(blower.Name);
            dao.setBlowerOn(blower.fanOn);
            dao.setBlowerRunTime(blower.totalRuntime);
        }
        else
        {
            dao.setBlowerID("");
            dao.setBlowerName("");
            dao.setBlowerOn(false);
            dao.setBlowerRunTime(0);
        }
        return dao;
    }
    
    public static Device convertDeviceDAOToDevice( DeviceDAO device )
    {
       Device d = new Device();
       
       if (device.getId().compareTo(device.getBlowerID()) == 0)
       {
           // Device is blower
           d = (Device) convertDeviceDAOToBlower(device);
       }
       else if ( device.getBlowerID().length() > 0 )
       {
           // Pit probe
           d = (Device) convertDeviceDAOToPitProbe(device);
       }
       else 
       {
           // Regular old Probe
           d = (Device) convertDeviceDAOToProbe(device);
       }
       return d;
    }
    
    public static Blower convertDeviceDAOToBlower( DeviceDAO device)
    {
        Blower b = new Blower();
        
        b.cooker = "";
        b.Name = device.getBlowerName();
        b.fanOn = device.isBlowerOn();
        b.id = device.getBlowerID();
        b.totalRuntime = device.getBlowerRunTime();
        
        return b;
    }
    
    public static Probe convertDeviceDAOToProbe( DeviceDAO device )
    {
        Probe p = new Probe();
        
        p.alarmType = device.getAlarmType();
        p.cooker = device.getCooker();
        p.currentTemp = String.valueOf(device.getCurrentTemp());
        p.id = device.getId();
        p.lowerTempAlarm = String.valueOf(device.getLowerTempAlarm());
        p.upperTempAlarm = String.valueOf(device.getUpperTempAlarm());
        p.Name = device.getName();
        p.targetTemp = String.valueOf(device.getTargetTemp());
       
       return p; 
    }
    
    public static PitProbe convertDeviceDAOToPitProbe( DeviceDAO device )
    {
        PitProbe p = new PitProbe();
        
        p.alarmType = device.getAlarmType();
        p.cooker = device.getCooker();
        p.currentTemp = String.valueOf(device.getCurrentTemp());
        p.id = device.getId();
        p.lowerTempAlarm = String.valueOf(device.getLowerTempAlarm());
        p.upperTempAlarm = String.valueOf(device.getUpperTempAlarm());
        p.Name = device.getName();
        p.targetTemp = String.valueOf(device.getTargetTemp());
       
        Blower b = new Blower();
        b.cooker = "";
        b.Name = device.getBlowerName();
        b.fanOn = device.isBlowerOn();
        b.id = device.getBlowerID();
        b.totalRuntime = device.getBlowerRunTime();
 
        p.blower = b;
        
       return p; 
    }
    
    public static float floatValueOf( String s )
    {
        float f = 0;
        try
        {
            f = Float.valueOf(s);
        }
        catch ( NumberFormatException nfe )
        {
            
        }
        return f;
        
    }
    
    public static int intValueOf( String s )
    {
        int i = 0;
        try
        {
           double d = Double.parseDouble(s);
           i = (int) d;
            //i = Integer.valueOf(s);
        }
        catch ( NumberFormatException nfe )
        {
            
        }
        return i;
    }

}
