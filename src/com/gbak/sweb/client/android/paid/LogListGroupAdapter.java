package com.gbak.sweb.client.android.paid;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.gbak.sweb.client.android.paid.ProbeItemAdapter.ViewHolder;
import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.services.StokerWebDataService;
import com.gbak.sweb.client.android.paid.services.exceptions.NoActiveSettingException;
import com.gbak.sweb.client.android.paid.services.exceptions.UnableToConnectException;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.json.Alert;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.LogItem;
import com.gbak.sweb.common.json.LogNote;
import com.gbak.sweb.common.json.PitProbe;
import com.gbak.sweb.common.json.Probe;

import com.gbak.sweb.client.android.paid.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LogListGroupAdapter extends BaseExpandableListAdapter
{

    private Context context;
    private ArrayList<LogItem> groups;
    private LayoutInflater m_Inflater;
    
    private static final String TAG = "LogListGroupAdapter";

    public LogListGroupAdapter(Context context, ArrayList<LogItem> groups)
    {
        this.context = context;
        this.groups = groups;
    }

    public void update( ArrayList<LogItem> groups)
    {
        this.groups = groups;
    }
    
    public void addItem(Device item, LogItem group)
    {
        if (!groups.contains(group))
        {
            groups.add(group);
        }
        int index = groups.indexOf(group);
        ArrayList<Device> ch = groups.get(index).deviceList;
        ch.add(item);
        groups.get(index).deviceList = ch;
    }

    public Object getChild(int groupPosition, int childPosition)
    {
        ArrayList<Device> chList = groups.get(groupPosition).deviceList;
        return chList.get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    public View getChildView(int groupPosition, int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent)
    {
        Device currentDevice = (Device) getChild(groupPosition, childPosition);

        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.probe_list_item, parent, false);
        }

        View v = convertView;
        ViewHolder holder = null;

        HashMap<String, ProbeAlert> alertMap = SWUtil.getProbeAlertMap();

       // if (convertView == null)
       //     v = m_Inflater.inflate(R.layout.probe_list_item, parent, false);
        if (currentDevice != null)
        {
            TextView probeDescription = (TextView) v
                    .findViewById(R.id.probeDescription);
            probeDescription.setText(currentDevice.Name);
            TextView probeTemp = (TextView) v.findViewById(R.id.temp);
            if (currentDevice instanceof Probe)
            {
                Probe currentProbe = (Probe) currentDevice;

                Log.e(TAG, "Probe Devices: " + currentDevice.Name);
                String temp = currentProbe.currentTemp;
                if (temp.contains("."))
                    temp = temp.substring(0, temp.indexOf("."));
                String tempStr = String.format("%3s", temp);
                probeTemp.setText(tempStr);

                // Alarm SettingsDAO

                TextView alarmHighLabel = (TextView) v
                        .findViewById(R.id.probe_list_alarm_h_label);
                TextView alarmHighData = (TextView) v
                        .findViewById(R.id.probe_list_alarm_h_data);
                TextView alarmLowLabel = (TextView) v
                        .findViewById(R.id.probe_list_alarm_l_label);
                TextView alarmLowData = (TextView) v
                        .findViewById(R.id.probe_list_alarm_l_data);

                if (currentProbe.alarmType == AlarmType.ALARM_FIRE)
                {
                    alarmHighLabel.setText(" H");
                    String alarmHigh = String.format("%3s",
                            currentProbe.upperTempAlarm);
                    alarmHighData.setText(alarmHigh);
                    alarmLowLabel.setText(" L");
                    String alarmLow = String.format("%3s",
                            currentProbe.lowerTempAlarm);
                    alarmLowData.setText(alarmLow);
                    alarmHighLabel.setVisibility(View.VISIBLE);
                    alarmHighData.setVisibility(View.VISIBLE);
                    alarmLowLabel.setVisibility(View.VISIBLE);
                    alarmLowData.setVisibility(View.VISIBLE);
                }
                else
                {
                    alarmHighLabel.setVisibility(View.GONE);
                    alarmHighData.setVisibility(View.GONE);
                    alarmLowLabel.setVisibility(View.GONE);
                    alarmLowData.setVisibility(View.GONE);
                }

                TextView targetLabel = (TextView) v
                        .findViewById(R.id.probe_list_target_label);
                TextView targetData = (TextView) v
                        .findViewById(R.id.probe_list_target_data);

                if ((currentProbe instanceof PitProbe && ((PitProbe) currentDevice).blower != null)
                        || currentProbe.alarmType == AlarmType.ALARM_FOOD)
                {
                    targetLabel.setText(" T");
                    String target = String.format("%3s",
                            currentProbe.targetTemp);
                    targetData.setText(target);
                    targetLabel.setVisibility(View.VISIBLE);
                    targetData.setVisibility(View.VISIBLE);
                }
                else
                {
                    targetLabel.setVisibility(View.GONE);
                    targetData.setVisibility(View.GONE);
                }
                ImageView fanImage = (ImageView) v.findViewById(R.id.fan_image);
                if (currentProbe instanceof PitProbe
                        && ((PitProbe) currentDevice).blower != null)
                {
                    fanImage.setVisibility(View.VISIBLE);
                    if (((PitProbe) currentDevice).blower.fanOn == true)
                    {
                        // RotateAnimation anim = new RotateAnimation(0f, 350f,
                        // 15f, 15f);
                        // anim.setInterpolator(new LinearInterpolator());
                        // anim.setRepeatCount(Animation.INFINITE);
                        // anim.setDuration(700);

                        // Start animating the image
                        // fanImage.startAnimation(anim);

                        Animation rotate = AnimationUtils.loadAnimation(
                                context, R.anim.rotate_indefinitely);
                        rotate.setInterpolator(new LinearInterpolator());
                        fanImage.startAnimation(rotate);
                    }
                    else
                        fanImage.setAnimation(null);
                }
                else
                    fanImage.setVisibility(View.GONE);

                View v2 = v.findViewById(R.id.probe_list_item);
                // Log.d(TAG, "ID: " + v2.getId());
                Alert alert = SWUtil.checkAlarmsOnDevice(alertMap,
                        currentDevice);
                if (alert != null && alert.id != null)
                {
                    // Log.d(TAG, "2ID: " + v2.getId());
                    // Log.d(TAG,"Position: " + position );
                    // Log.d(TAG,"Alarm: " + alert.name + " message: " +
                    // alert.message );
                    // Log.d(TAG,"Probe: " +
                    // probeDescription.getText().toString());
                    //
                    TransitionDrawable transition = (TransitionDrawable) v2
                            .getBackground();
                    transition.resetTransition();
                    transition.startTransition(2000);

                }
                else
                {
                    TransitionDrawable transition = (TransitionDrawable) v2
                            .getBackground();
                    transition.resetTransition();
                }
            }

        }
        /*
         * boolean isSet = alarmIndexes.contains(new Integer(position)); Alert
         * alert = SWUtil.checkAlarmsOnDevice( alertMap, currentDevice ); if (
         * alert != null && !isSet ) alarmIndexes.add( new Integer( position ));
         * else if ( alert == null && isSet ) alarmIndexes.remove(new Integer(
         * position ));
         */

        return v;

    }

    public int getChildrenCount(int groupPosition)
    {
        ArrayList<Device> chList = groups.get(groupPosition).deviceList;
        return chList.size();
    }

    public Object getGroup(int groupPosition)
    {
        return groups.get(groupPosition);
    }

    public int getGroupCount()
    {
        return groups.size();
    }

    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }


    public View getGroupView(int groupPosition, boolean isLastChild, View view,
            ViewGroup parent)
    {
        final LogItem group = (LogItem) getGroup(groupPosition);
        if (view == null)
        {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.log_list_group_item, null);
        }
        TextView durationLabel = (TextView) view.findViewById(R.id.logHeaderCookerTextView);
        durationLabel.setText(group.logName);
       
        long durationMin = ( Calendar.getInstance().getTimeInMillis() - group.startDate.getTime() ) / 60000;
        int minutes = (int)durationMin % 60;
        int hours = (int)(durationMin - minutes) / 60;
       // int hours = (int)TimeUnit.MILLISECONDS.toHours(durationMills); // API 9 only
      
        String durationString = "";
        if ( hours >= 1 )
            durationString = hours + " h ";
        durationString += minutes + " m ";    
        
        TextView durationValue = (TextView) view.findViewById(R.id.logHeaderDurationTimeTextView);
        durationValue.setText(durationString);
        
        Button buttonNewNote = (Button) view.findViewById(R.id.button_add_note);
        buttonNewNote.setFocusable(false);
        Button buttonEndLog = (Button) view.findViewById(R.id.button_end_log );
        buttonEndLog.setFocusable(false);  // without this the ExpandableList will not expand
        if ( group.logName.startsWith("Default"))
            buttonEndLog.setEnabled(false);
        else
        {
            buttonEndLog.setEnabled(true);
        
           buttonEndLog.setOnClickListener( new OnClickListener()
           {
               public void onClick(View v)
               {
                   new AlertDialog.Builder(context)
                   .setTitle("End Log")
                   .setMessage("Stop log: \""+ group.logName + "\"?")
                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() 
                   {
                       public void onClick(DialogInterface dialog, int whichButton) 
                       {
                           LogItem[] lia = new LogItem[] { group };
                           new EndLogTask().execute(lia );
                       }
                   }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
                   {
                       public void onClick(DialogInterface dialog, int whichButton) { }
                   }).show();
               }
           } );
        }
        buttonNewNote.setOnClickListener( new OnClickListener()
        {
            public void onClick(View v)
            {
        
             //   final ArrayList<Integer> selectedItems = new ArrayList<Integer>();
                final HashMap<Integer,LogItem> selectedItemHash = new HashMap<Integer,LogItem>();
               
                final EditText input = new EditText(context);
                input.setMaxLines(5);
                input.setHint("Note");
               
                int i = 0;
                boolean[] selectedArray = new boolean[groups.size()];
                CharSequence[] groupArray = new CharSequence[groups.size()];
                for ( LogItem g : groups )
                {
                    groupArray[i] = g.logName;
                    if ( group.logName.compareTo(g.logName) == 0)
                    {
                        selectedArray[i] = true;
                        selectedItemHash.put(Integer.valueOf(i), groups.get(i));
                    }
                    else
                        selectedArray[i] = false;
                    i++;
                }
                
              //  NoteLogChooser nlc = new NoteLogChooser(context);
               // final EditText input = new EditText(context);
                new AlertDialog.Builder(context)
                .setTitle("Add Note")
               // .setMessage("End currently running log?")
                .setMultiChoiceItems(groupArray, selectedArray, new DialogInterface.OnMultiChoiceClickListener() {
                 
                    public void onClick(DialogInterface dialog, int which,
                            boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            //selectedItems.add(which);
                            selectedItemHash.put(Integer.valueOf(which), groups.get(which));
                        } else if (selectedItemHash.containsKey(Integer.valueOf(which))) {
                            // Else, if the item is already in the array, remove it 
                            //selectedItems.remove(Integer.valueOf(which));
                            selectedItemHash.remove(Integer.valueOf(which));
                        }
                    }
                })
               .setView(input)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() 
                {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                        ArrayList<String> logList = new ArrayList<String>();
                        for ( LogItem li : selectedItemHash.values())
                        {
                           logList.add( li.logName);    
                        }
                        
                        LogNote ln = new LogNote(input.getText().toString(), logList );
                        LogNote[] lna = new LogNote[] { ln };
                        new NewNoteTask().execute(lna);
                        // COnvert the selectedItems array to LogItem list and submit
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
                {
                    public void onClick(DialogInterface dialog, int whichButton) 
                    {
                        // Do nothing.
                    }
                }).show();
            }
        } );
        
        return view;
    }
    
    public boolean hasStableIds()
    {
        return true;
    }

    public boolean isChildSelectable(int arg0, int arg1)
    {
        return true;
    }
    
    private class EndLogTask extends AsyncTask<LogItem, Void, Boolean>
    {
        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
                Toast.makeText(StokerWebAndroid.getContext(), "Save Successful", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(StokerWebAndroid.getContext(), "Save Failed", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(LogItem... li)
        {
            try
            {
                StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;
                swData.endLog(li[0]);
            }
            catch (UnableToConnectException uce)
            {
                Log.e("EndLogTask","Unable to make connection to save configuration");
                return false;
            }
            catch (NoActiveSettingException e)
            {
                Log.e("EndLogTask","No Active Settings Caught when attempting to end log");
                return false;             
            }
            return true;
        }
    }

    private class NewNoteTask extends AsyncTask<LogNote, Void, Boolean>
    {
        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
                Toast.makeText(StokerWebAndroid.getContext(), "Note added", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(StokerWebAndroid.getContext(), "Note add failed", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Boolean doInBackground(LogNote... li)
        {
            try
            {
                StokerWebDataService swData = StokerWebAndroid.getServices().stokerWebDataService;
                swData.newNote(li[0]);
            }
            catch (UnableToConnectException uce)
            {
                Log.e("NewNoteTask","Unable to make connection to save Note");
                return false;
            }
            catch (NoActiveSettingException e)
            {
                Log.e("NewNoteTask","NoActiveSettingsException caught when trying to save note");
                return false;             
            }
            return true;
        }
    }

   

}
