package com.gbak.sweb.client.android.paid;

import java.util.HashMap;

import com.gbak.sweb.client.android.paid.data.dao.ProbeAlert;
import com.gbak.sweb.client.android.paid.utils.SWUtil;
import com.gbak.sweb.common.base.constant.AlarmType;
import com.gbak.sweb.common.json.Alert;
import com.gbak.sweb.common.json.Device;
import com.gbak.sweb.common.json.DeviceDataList;
import com.gbak.sweb.common.json.PitProbe;
import com.gbak.sweb.common.json.Probe;
import com.gbak.sweb.common.json.Blower;

import com.gbak.sweb.client.android.paid.R;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.drawable.TransitionDrawable;


public class ProbeItemAdapter extends ArrayAdapter<Device>
{  
    public static String TAG = "ProbeItemAdapter";
    
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;
    
    private DeviceDataList deviceList = new DeviceDataList();
    private HashMap<String,Device> deviceHash = new HashMap<String,Device>();
    private LayoutInflater m_Inflater;
    private Context ctx;
    

    
    public ProbeItemAdapter( Context context, 
                             int textViewResourceId,
                             DeviceDataList devices)
    {
        super(context, textViewResourceId, devices.devices);
        this.deviceList = devices;
        ctx = context;
//        for ( DeviceDAO d : devices.devices )
//           deviceHash.put( d.id,d);
        
        
        m_Inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public void animateAlertedItems()
    {
        Log.d(TAG,"animating Alerted Items");
        
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Device currentDevice = deviceList.devices.get(position);
        
        View v = convertView;
        ViewHolder holder = null;
        int type = getItemViewType(position);
        HashMap<String,ProbeAlert> alertMap = SWUtil.getProbeAlertMap();

        
        switch( type )
        {
            case TYPE_ITEM:
                if ( convertView == null)
                   v = m_Inflater.inflate(R.layout.probe_list_item, parent, false);
                if (currentDevice != null)
                {
                    TextView probeDescription = (TextView) v
                            .findViewById(R.id.probeDescription);
                    probeDescription.setText(currentDevice.Name);
                    TextView probeTemp = (TextView) v.findViewById(R.id.temp);
                    if ( currentDevice instanceof Probe )
                    {
                        Probe currentProbe = (Probe) currentDevice;
                        
                        String temp = currentProbe.currentTemp;
                        if ( temp.contains("."))
                            temp = temp.substring(0, temp.indexOf("."));
                        String tempStr = String.format("%3s", temp );
                        probeTemp.setText(tempStr);
                        
                        // Alarm SettingsDAO
        
        
                        TextView alarmHighLabel = (TextView) v.findViewById(R.id.probe_list_alarm_h_label);
                        TextView alarmHighData = (TextView) v.findViewById(R.id.probe_list_alarm_h_data);
                        TextView alarmLowLabel = (TextView) v.findViewById(R.id.probe_list_alarm_l_label);
                        TextView alarmLowData = (TextView) v.findViewById(R.id.probe_list_alarm_l_data);
        
                        if ( currentProbe.alarmType == AlarmType.ALARM_FIRE)
                        {
                            alarmHighLabel.setText( " H");
                            double highd = Double.valueOf(currentProbe.upperTempAlarm);
                            String alarmHigh = String.format("%3d", (int)highd );
                            alarmHighData.setText( alarmHigh );
                            alarmLowLabel.setText( " L");
                            double highl = Double.valueOf(currentProbe.lowerTempAlarm);
                            String alarmLow = String.format("%3d", (int)highl );
                            alarmLowData.setText( alarmLow);
                            alarmHighLabel.setVisibility(View.VISIBLE);
                            alarmHighData.setVisibility(View.VISIBLE);
                            alarmLowLabel.setVisibility(View.VISIBLE);
                            alarmLowData.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            alarmHighLabel.setVisibility(View.GONE);
                            alarmHighData.setVisibility(View.GONE);
                            alarmLowLabel.setVisibility(View.GONE);
                            alarmLowData.setVisibility(View.GONE);
                        }
                        
                        TextView targetLabel = (TextView) v.findViewById(R.id.probe_list_target_label);
                        TextView targetData = (TextView) v.findViewById(R.id.probe_list_target_data);
                        
                        if (( currentProbe instanceof PitProbe && ((PitProbe)currentDevice).blower != null )|| 
                             currentProbe.alarmType == AlarmType.ALARM_FOOD )
                        {
                            targetLabel.setText( " T");
                            double doubleTargetTemp = Double.valueOf(currentProbe.targetTemp);
                            
                            String target = String.format("%3d", (int)doubleTargetTemp );
                            targetData.setText( target);
                            targetLabel.setVisibility(View.VISIBLE);
                            targetData.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            targetLabel.setVisibility(View.GONE);
                            targetData.setVisibility(View.GONE);
                        }
                        ImageView fanImage = (ImageView) v.findViewById(R.id.fan_image);
                        if ( currentProbe instanceof PitProbe && ((PitProbe)currentDevice).blower != null )
                        {
                            fanImage.setVisibility(View.VISIBLE);
                            if ( ((PitProbe)currentDevice).blower.fanOn == true )
                            {
                                Animation rotate = AnimationUtils.loadAnimation(ctx, R.anim.rotate_indefinitely);
                                rotate.setInterpolator(new LinearInterpolator());
                                fanImage.startAnimation(rotate);
                            }
                            else
                            {
                                if ( fanImage.getAnimation() != null )
                                fanImage.getAnimation().cancel();
                            }
                        }
                        else
                            fanImage.setVisibility(View.GONE);
                        
                        View v2 = v.findViewById(R.id.probe_list_item);
                     //   Log.d(TAG, "ID: " + v2.getId());
                        Alert alert = SWUtil.checkAlarmsOnDevice( alertMap, currentDevice );
                        if ( alert != null && alert.id != null )
                        {
                       //     Log.d(TAG, "2ID: " + v2.getId());
                       //     Log.d(TAG,"Position: " + position );
                       //     Log.d(TAG,"Alarm: " + alert.name + " message: " + alert.message );
                       //     Log.d(TAG,"Probe: " + probeDescription.getText().toString());
//                            
                            TransitionDrawable transition = (TransitionDrawable) v2.getBackground();
                            transition.resetTransition();
                            transition.startTransition(2000);
                            
                        }
                        else
                        {
                            TransitionDrawable transition = (TransitionDrawable) v2.getBackground();
                            transition.resetTransition();
                        }
                    }
                }
/*                boolean isSet = alarmIndexes.contains(new Integer(position));
                Alert alert = SWUtil.checkAlarmsOnDevice( alertMap, currentDevice );
                if ( alert != null && !isSet )
                    alarmIndexes.add( new Integer( position ));
                else if ( alert == null && isSet )
                    alarmIndexes.remove(new Integer( position ));*/
                
                
                break;
            case TYPE_SEPARATOR:
                if ( convertView == null )
                   v = m_Inflater.inflate(R.layout.probe_list_cooker_heading, null);  //null
                TextView cookerHeader = (TextView) v.findViewById(R.id.cookerName);
                cookerHeader.setText(currentDevice.cooker);
                
                Button logsButton = (Button) v.findViewById(R.id.heading_logs_button );
                
                final String cookerName = currentDevice.cooker;
                logsButton.setOnClickListener(new OnClickListener() {

                    public void onClick(View arg0)
                    {
                        Log.e(TAG,"Button Click for: " + cookerName);
                        
                        Intent logIntent = new Intent( ctx, LogActivity.class);
                        logIntent.putExtra("cookerName", cookerName );
                        ctx.startActivity( logIntent );
                        
                        }
                    
                });
                
                Integer logCount = null;
                if ( deviceList.logCount != null)  // this can be null when switching from stoker-web to stoker on the fly.
                   logCount = deviceList.logCount.logItemCount.get(currentDevice.cooker);
                
                String buttonText = "";
                if ( logCount == null )
                    buttonText = "Logs (0)";
                else
                    buttonText = "Logs (" + String.valueOf(logCount.intValue()) + ")";
                    logsButton.setText( buttonText );
                    
                //logsButton.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFFAA0000));
               // logsButton.getBackground().setColorFilter(0xFF00FF00, android.graphics.PorterDuff.Mode.MULTIPLY);  // Green
                logsButton.getBackground().setColorFilter(0xFF888888, android.graphics.PorterDuff.Mode.MULTIPLY);
                break;
                
        }

        
        return v;
    }

    @Override
    public void notifyDataSetChanged()
    {
        Log.d(TAG,"Dataset changed called");
        super.notifyDataSetChanged();
        
    }
        
    @Override
    public int getItemViewType(int position) 
    {
        return deviceList.devices.get(position).id == null ? TYPE_SEPARATOR : TYPE_ITEM;
        
    }
    
    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }
    
    public int getCount()
    {
        if ( deviceList == null )
            return 0;
        
        return deviceList.devices.size();
    }
    
    public static class ViewHolder {
        public TextView textView;
    }
    
    public void update(DeviceDataList devices )
    {
        this.deviceList = devices;
    }
    
   /* private class CheckAlarmsTask extends AsyncTask<Void, Void, Boolean>
    {

        @Override
        protected void onPostExecute(Boolean result)
        {
            if (result)
            {
                Toast.makeText(StokerWebAndroid.getContext(),
                        "Save Successful", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(StokerWebAndroid.getContext(), "Save Failed",
                        Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... arg0)
        {
            try
            {
               
            }
            catch (UnableToConnectException uce)
            {
                
                return false;
            }
            return true;
        }
    }*/
}
